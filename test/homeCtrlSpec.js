(function () {
	'use strict';

	describe('Home Controller', function () {
		var ctrl, scope, store;

		beforeEach(module('lenspireApp'));

		beforeEach(inject(function ($controller, $rootScope, localStorage) {
			

			ctrl = $controller('HomeCtrl', {
				$scope: scope
			});
		}));

		it('text is not null', function () {
		    expect(scope.text).toBeNull();
		});

		it('text is equal to  Lenspire', function () {
		    expect(scope.text).toBe('Lenspire');
		});

	});
}());
