
var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    del = require('del'),
    sass = require('gulp-sass'),
    karma = require('gulp-karma'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    spritesmith = require('gulp.spritesmith'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    ngAnnotate = require('browserify-ngannotate');

var CacheBuster = require('gulp-cachebust');
var cachebust = new CacheBuster();

var config = require('./gulp-config.json');

// cleans the build output

gulp.task('clean', function (cb) {
    del([
        config.paths.dist
    ], cb);
});

// runs bower to install frontend dependencies

gulp.task('bower', function () {

    var install = require("gulp-install");

    return gulp.src(['./bower.json'])
        .pipe(install());
});

// concat Vendor js
gulp.task('vendor-js', ['clean', 'bower'], function () {
    return gulp
        .src(config.paths.vendorjs)
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest(config.paths.vendor));
});

// concat ui js
gulp.task('ui-js', ['clean', 'bower'], function () {
    return gulp
        .src(config.paths.uijs)
        .pipe(concat('ui.min.js'))
        .pipe(gulp.dest(config.paths.vendor));
});

// concat Vendor css
gulp.task('vendor-css', ['clean', 'bower'], function () {
    return gulp
        .src(config.paths.vendorcss)
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(config.paths.vendor));
});

// copy images
gulp.task('copy-image', ['clean'], function () {
    return gulp
        .src('./images/**')
        .pipe(gulp.dest(config.paths.images));
});

// copy images
gulp.task('copy-fonts', ['clean'], function () {
    return gulp
        .src('./fonts/*')
        .pipe(gulp.dest(config.paths.fonts));
});


// runs sass, creates css source maps

gulp.task('build-css', ['clean'], function () {
    return gulp.src('./styles/*')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(config.paths.styles));
});

// fills in the Angular template cache, to prevent loading the html templates via
// separate http requests

gulp.task('build-template-cache', ['clean'], function () {

    var ngHtml2Js = require("gulp-ng-html2js");

    return gulp.src("./partials/**")
        .pipe(ngHtml2Js({
            moduleName: "partialsModule",
            prefix: "/partials/"
        }))
        .pipe(concat("template.js"))
        .pipe(gulp.dest(config.paths.scripts));
});

// runs jshint

gulp.task('jshint', function () {
    gulp.src('/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// runs karma tests

gulp.task('test', ['build-js'], function () {
    var testFiles = [
        './test/unit/*.js'
    ];

    return gulp.src(testFiles)
        .pipe(karma({
            configFile: 'karma.conf.js',
            action: 'run'
        }))
        .on('error', function (err) {
            console.log('karma tests failed: ' + err);
            throw err;
        });
});

// Build a minified Javascript bundle - the order of the js files is determined
// by browserify

gulp.task('build-js', ['clean'], function () {
    var b = browserify({
        entries: './js/app.js',
        debug: true,        
        paths: ['./js/controllers', './js/services', './js/directives'],
        transform: [ngAnnotate]
    });
    if (config.debug)
        return b.bundle()
            .pipe(source('bundle.js'))
            .pipe(buffer())
            .pipe(cachebust.resources())
            .pipe(sourcemaps.init({ loadMaps: true }))
            .on('error', gutil.log)
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(config.paths.scripts));
    else
        return b.bundle()
           .pipe(source('bundle.js'))
           .pipe(buffer())
           .pipe(cachebust.resources())
           .pipe(sourcemaps.init({ loadMaps: true }))
           .pipe(uglify())
           .on('error', gutil.log)
           .pipe(sourcemaps.write('./'))
           .pipe(gulp.dest(config.paths.scripts));
});


// full build applies cache busting to the main page css and js bundles

gulp.task('build', ['clean', 'bower', 'sprite', 'vendor-js', 'ui-js', 'vendor-css', 'copy-image', 'copy-fonts', 'build-css', 'build-template-cache', 'jshint', 'build-js'], function () {
    return gulp.src(['*.html'])
        .pipe(cachebust.references())
        .pipe(gulp.dest(config.paths.dist));
});

// watches file system and triggers a build when a modification is detected

gulp.task('watch', function () {
    return gulp.watch(['./*.html', './partials/**/**.html', './styles/*.*css', './js/**/**.js'], ['build']);
});

// launches a web server that serves files in the current directory

gulp.task('webserver', ['watch', 'build'], function () {
    gulp.src('.')
        .pipe(webserver({
            livereload: false,
            directoryListing: true,
            open: "http://localhost:8000/dist/index.html"
        }));
});

// launch a build upon modification and publish it to a running server

gulp.task('dev', ['watch', 'webserver']);

// generates a sprite png and the corresponding sass sprite map.
// This is not included in the recurring development build and needs to be run separately

gulp.task('sprite',['clean'] , function () {

    var spriteData = gulp.src('./icon/*.png')
        .pipe(spritesmith({
            imgName: 'icon-sprite.png',
            cssName: 'icon-sprite.css',
            algorithm: 'top-down',
            padding: 5
        }));

    spriteData.css.pipe(gulp.dest(config.paths.styles));
    spriteData.img.pipe(gulp.dest(config.paths.styles))
});
// installs and builds everything

gulp.task('default', ['build']);