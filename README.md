lenspire UI
=================

# Installation instructions

**Prerequisites - Node.js**

First make sure Gulp is globally installed, by running:

	npm install -g bower
    npm install -g gulp

After cloning the project, run the following commands:

	bower install
    npm install
    gulp
    gulp dev
    
This will start the development server, the lenspire UI app should be available at the following url:

[http://localhost:8000/dist/index.html](http://localhost:8000/dist/index.html)

The necessary ui files are located in the /dist folder.
    
