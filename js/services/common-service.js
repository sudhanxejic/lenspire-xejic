﻿angular.module('lenspireApp')
    .provider('CommonService', function () {

        this.$get = function () {
            return this;
        };

        this.getUser = function () {
            var user = JSON.parse(localStorage.getItem("USER"));
            return user;
        }

        this.getProductinBooks = function () {
            var productinBooks = JSON.parse(localStorage.getItem("PRODUCTINBOOK"));
            if (!productinBooks)
                productinBooks = [{
                    id: 1,
                    logo: ['images/data/logo-1.png', 'images/data/logo-2.png', 'images/data/logo-3.png', 'images/data/logo-4.png'],
                    bookName: "ALPHA PROJECT",
                    jobNo: "001",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["John Smith"],
                    agency: ["BRYNN CREATIVE", "Ross Photography", "Vanity Model"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 1,
                    publish : true,
                    crews: [{ id: 2, name: 'Heather Greene', profile: 'images/data/avatar-11.png' }, { id: 4, name: 'Lili Regen', profile: 'images/data/avatar-13.png' }]
                }, {
                    id: 2,
                    logo: ['images/data/logo-2.png', 'images/data/logo-1.png', 'images/data/logo-3.png', 'images/data/logo-4.png'],
                    bookName: "BETA PROJECT",
                    jobNo: "002",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["John Doe"],
                    agency: ["BRYNN CREATIVE", "Ross Photography", "Vanity Model"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 1,
                    publish: false,
                    crews: [{ id: 2, name: 'Heather Greene', profile: 'images/data/avatar-11.png' }]
                }, {
                    id: 3,
                    logo: ['images/data/logo-3.png', 'images/data/logo-1.png', 'images/data/logo-2.png', 'images/data/logo-4.png'],
                    bookName: "DEMO PROJECT",
                    jobNo: "003",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["Lois Lane"],
                    agency: ["BRYNN CREATIVE", "Ross Photography"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 1,
                    publish: false,
                    crews: []
                }, {
                    id: 4,
                    logo: ['images/data/logo-1.png', 'images/data/logo-2.png', 'images/data/logo-3.png', 'images/data/logo-4.png'],
                    bookName: "ALPHA PROJECT",
                    jobNo: "001",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["John Smith"],
                    agency: ["BRYNN CREATIVE", "Ross Photography", "Vanity Model"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 2,
                    publish: true,
                    crews: [{ id: 2, name: 'Heather Greene', profile: 'images/data/avatar-11.png' }, { id: 4, name: 'Lili Regen', profile: 'images/data/avatar-13.png' }]
                }, {
                    id: 5,
                    logo: ['images/data/logo-2.png', 'images/data/logo-1.png', 'images/data/logo-3.png', 'images/data/logo-4.png'],
                    bookName: "BETA PROJECT",
                    jobNo: "002",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["John Doe"],
                    agency: ["BRYNN CREATIVE", "Ross Photography", "Vanity Model"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 2,
                    publish: false,
                    crews: [{ id: 2, name: 'Heather Greene', profile: 'images/data/avatar-11.png' }]
                }, {
                    id: 6,
                    logo: ['images/data/logo-3.png', 'images/data/logo-2.png', 'images/data/logo-4.png', 'images/data/logo-1.png'],
                    bookName: "GAMMA PROJECT",
                    jobNo: "003",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["Lois Lane"],
                    agency: ["BRYNN CREATIVE", "Ross Photography"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 2,
                    publish: false,
                    crews: []
                }, {
                    id: 7,
                    logo: ['images/data/logo-1.png', 'images/data/logo-2.png', 'images/data/logo-3.png', 'images/data/logo-4.png'],
                    bookName: "ALPHA PROJECT",
                    jobNo: "001",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["John Smith"],
                    agency: ["BRYNN CREATIVE", "Ross Photography", "Vanity Model"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 3,
                    publish: true,
                    crews: [{ id: 2, name: 'Heather Greene', profile: 'images/data/avatar-11.png' }, { id: 4, name: 'Lili Regen', profile: 'images/data/avatar-13.png' }]
                }, {
                    id: 8,
                    logo: ['images/data/logo-2.png', 'images/data/logo-4.png', 'images/data/logo-3.png', 'images/data/logo-1.png'],
                    bookName: "BETA PROJECT",
                    jobNo: "002",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["John Doe"],
                    agency: ["BRYNN CREATIVE", "Ross Photography", "Vanity Model"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 3,
                    publish: false,
                    crews: ["Heather Greene"]
                }, {
                    id: 9,
                    logo: ['images/data/logo-3.png', 'images/data/logo-2.png', 'images/data/logo-4.png', 'images/data/logo-1.png'],
                    bookName: "GAMMA PROJECT",
                    jobNo: "003",
                    startDate: new Date(2016, 6, 20, 18, 25, 0, 0),
                    endDate: new Date(2016, 6, 31, 18, 25, 0, 0),
                    client: ["Lois Lane"],
                    agency: ["BRYNN CREATIVE", "Ross Photography"],
                    shootNotes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    billingInfo: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est.",
                    usageTerms: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris   a dui molestie, porttitor sapien quis, venenatis turpis.",
                    startDate1: new Date(2016, 6, 28, 18, 25, 0, 0),
                    endDate1: new Date(2016, 6, 31, 18, 25, 0, 0),
                    status: 3,
                    publish: false,
                    crews: []
                }];
            return productinBooks;
        }

        this.getProductinBook = function (productinBookID) {
            var productinBooks = this.getProductinBooks();
            var productinBook = null;
            for (var i = 0; i < productinBooks.length; i++) {
                if (productinBooks[i].id == productinBookID) {
                    productinBook = productinBooks[i];
                    break;
                }
            }
            return productinBook;
        }

        this.setProductinBook = function (productinBook) {
            var productinBooks = this.getProductinBooks();
            if (!productinBook.id) {
                productinBook.id = productinBooks.length + 1;
                productinBooks.push(productinBook);
            } else {
                for (var i = 0; i < productinBooks.length; i++) {
                    if (productinBooks[i].id == productinBook.id) {
                        productinBooks[i] = productinBook
                        break;
                    }
                }
            }
            localStorage.setItem("PRODUCTINBOOK", JSON.stringify(productinBooks));
        }



        this.getCrews = function () {
            var crews = JSON.parse(localStorage.getItem("CREWS"));
            if (!crews) {
                crews = [{ id: 1, name: "1st Photographer Assistant" }, { id: 2, name: "2nd Photographer Assistant" }, { id: 3, name: "Account executive" }, { id: 4, name: "Actor" }, { id: 5, name: "Advertising Agency" }, { id: 6, name: "Agent" }, { id: 7, name: "Animal Talent" }, { id: 8, name: " Animal Talent Agency" }, { id: 9, name: "Animation" }, { id: 10, name: "Ariel Operator" }, { id: 11, name: "Art Buyer" }, { id: 12, name: "Art Director" }, { id: 13, name: "Artist Representative Agency" }, { id: 14, name: "Assistant Director" }, { id: 15, name: "Audio Assistant" }, { id: 16, name: "Audio Visual Equipment Rental House" }, { id: 17, name: "B-Roll" }, { id: 18, name: "Best Boy Electric" }, { id: 19, name: "Best Boy Grip" }, { id: 20, name: "Boom Operator" }, { id: 21, name: "Brand" }, { id: 22, name: "Broadcast Engineer" }, { id: 23, name: "Broadcast/ Multi Camera Operator" }, { id: 24, name: "Camera Operator" }, { id: 25, name: "Camera Rental House" }, { id: 26, name: "Captioning" }, { id: 27, name: "Casting Agency" }, { id: 28, name: "Casting Agent" }, { id: 29, name: "Casting Director" }, { id: 30, name: "Catering" }, { id: 31, name: "Client" }, { id: 32, name: "Color Correction" }, { id: 33, name: "Comedian" }, { id: 34, name: "Composer" }, { id: 35, name: "Copywriter" }, { id: 36, name: "Costume Maker" }, { id: 37, name: "Costume Rental House" }, { id: 38, name: "Cosutme Designer" }, { id: 39, name: "Craft Services" }, { id: 40, name: "Crane/ Jib Operator" }, { id: 41, name: "Creative Director" }, { id: 42, name: "Creative Director" }, { id: 43, name: "Cue Card Writer" }, { id: 44, name: "Dallies" }, { id: 45, name: "Digital Equipment Rental House" }, { id: 46, name: "Digital Imaging Technician" }, { id: 47, name: "Digital Tech" }, { id: 48, name: "Dimmer Board Operator" }, { id: 49, name: "Director" }, { id: 50, name: "Director Of Photography" }, { id: 51, name: "Dolly Grip" }, { id: 52, name: "DVD/CD-Rom Authorizing" }, { id: 53, name: "DVD/CD-Rom Duplication" }, { id: 54, name: "Editor" }, { id: 55, name: "Editorial" }, { id: 56, name: "Electician" }, { id: 57, name: "Equipment Rental House" }, { id: 58, name: "Executive Producer" }, { id: 59, name: "Film Lab" }, { id: 60, name: "Film Producer" }, { id: 61, name: "Floor Manager" }, { id: 62, name: "Food Stylist" }, { id: 63, name: "Furniture Rental House" }, { id: 64, name: "Gaffer" }, { id: 65, name: "Graphic Designer" }, { id: 66, name: "Grip" }, { id: 67, name: "Groomer" }, { id: 68, name: "Hair & Makeup Artist" }, { id: 69, name: "Hair Artist" }, { id: 70, name: "Illustrator" }, { id: 71, name: "Jib Operator" }, { id: 72, name: "Key Grip" }, { id: 73, name: "Lettering Artist" }, { id: 74, name: "Light Board Operator" }, { id: 75, name: "Lighting Designer" }, { id: 76, name: "Line Coordinator" }, { id: 77, name: "Location Agency" }, { id: 78, name: "Location Manager" }, { id: 79, name: "Location Rental" }, { id: 80, name: "Location Scout" }, { id: 81, name: "Location Sound Mixer" }, { id: 82, name: "Location Supplies Rental House" }, { id: 83, name: "Magazine" }, { id: 84, name: "Makeup Artist" }, { id: 85, name: "Manicurist" }, { id: 86, name: "Mixing Facility" }, { id: 87, name: "Model" }, { id: 88, name: "Model Agency" }, { id: 89, name: "Model Agent" }, { id: 90, name: "Motion Control" }, { id: 91, name: "Mulit Camera Assistant" }, { id: 92, name: "Mulit Camera Operator" }, { id: 93, name: "Music Composition" }, { id: 94, name: "Music Editor" }, { id: 95, name: "Music Publisher" }, { id: 96, name: "Painter" }, { id: 97, name: "Parts Model" }, { id: 98, name: "Photographer" }, { id: 99, name: "Post Facility" }, { id: 100, name: "Post Production Assistant" }, { id: 101, name: "Post Production Supervisor" }, { id: 102, name: "Producer" }, { id: 103, name: "Production Assistant" }, { id: 104, name: "Production Company" }, { id: 105, name: "Production Coordinator" }, { id: 106, name: "Production Manager" }, { id: 107, name: "Production Supervisor" }, { id: 108, name: "Prop Rental House" }, { id: 109, name: "Prop Stylist" }, { id: 110, name: "Prop Stylist" }, { id: 111, name: "Pyrotechnics" }, { id: 112, name: "Recording Studio" }, { id: 113, name: "Retoucher" }, { id: 114, name: "Rigging Electric" }, { id: 115, name: "Rigging Gaffer" }, { id: 116, name: "Rigging Grip" }, { id: 117, name: "Robo Operator" }, { id: 118, name: "Runner" }, { id: 119, name: "Satellite Communications" }, { id: 120, name: "Scoring" }, { id: 121, name: "Screen Writer" }, { id: 122, name: "Script Revisionist" }, { id: 123, name: "Seamstress" }, { id: 124, name: "Set Builder" }, { id: 125, name: "Sound Design" }, { id: 126, name: "Sound Editor" }, { id: 127, name: "Sound Equipment Rental" }, { id: 128, name: "Sound Mixing" }, { id: 129, name: "Sound Supervisor" }, { id: 130, name: "Special Effects Assistant" }, { id: 131, name: "Special Effects Foreman" }, { id: 132, name: "Special Effects Makeup" }, { id: 133, name: "Special Effects Prosthetics" }, { id: 134, name: "Special Effects Technician" }, { id: 135, name: "Stage Manager" }, { id: 136, name: "Stage Rental" }, { id: 137, name: "Still-Life Stylist" }, { id: 138, name: "Stock Agency" }, { id: 139, name: "Student" }, { id: 140, name: "Studio Rental" }, { id: 141, name: "Stunt Coordinator" }, { id: 142, name: "Stunt Driver" }, { id: 143, name: "Stunt Equipment Rental House" }, { id: 144, name: "Stunt Utility" }, { id: 145, name: "Swing" }, { id: 146, name: "Talent" }, { id: 147, name: "Talent Agency" }, { id: 148, name: "Talent Agent" }, { id: 149, name: "Tape/ Replay Operator" }, { id: 150, name: "Teacher" }, { id: 151, name: "Technical Director" }, { id: 152, name: "Translator" }, { id: 153, name: "Utility" }, { id: 154, name: "Video Assistant Company" }, { id: 155, name: "Video Assistant Operator" }, { id: 156, name: "Video Camera Operator- Eng" }, { id: 157, name: "Video Camera Operator- HD" }, { id: 158, name: "Video Engineer" }, { id: 159, name: "Video Operator" }, { id: 160, name: "Video Producer" }, { id: 161, name: "Wardrobe Stylist" }, { id: 162, name: "Full Service Production" }, { id: 163, name: "Modeling Agent" } ];
            }
            return crews;
        }

        this.getCrew = function (crewID) {
            var crews = this.getCrews();
            var crew = null;
            for (var i = 0; i < crews.length; i++) {
                if (crews[i].id == crewID) {
                    crew = crews[i];
                    break;
                }
            }
            return crew;
        }

        this.getCrewMembers = function (crewID) {
            var crews = this.getCrews();
            var crew = null;
            for (var i = 0; i < crews.length; i++) {
                if (crews[i].id == crewID) {
                    crew = crews[i];
                    break;
                }
            }
            return crew.members;
        }

        this.setCrew = function (crew) {
            var crews = this.getCrews();
            if (!crew.id) {
                crew.id = crews.length + 1;
                crews.push(crew);
            } else {
                for (var i = 0; i < crews.length; i++) {
                    if (crews[i].id == crew.id) {
                        crews[i] = crew
                        break;
                    }
                }
            }
            localStorage.setItem("CREWS", JSON.stringify(crews));
        }

    });