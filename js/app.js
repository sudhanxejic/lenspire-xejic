angular.module('lenspireApp', ['ngRoute', 'partialsModule', 'ui.bootstrap', 'dndLists', 'ngMaterial', 'ngFileUpload', 'ui.calendar'])
	.config(function ($routeProvider) {
	    'use strict';

	    var home = { controller: 'HomeCtrl', templateUrl: '/partials/home.html' };
	    var notification = { controller: 'NotificationCtrl', templateUrl: '/partials/notification.html' };
	    var messages = { controller: 'MessagesCtrl', templateUrl: '/partials/messages.html' };
	    var myartists = { controller: 'MyArtistsCtrl', templateUrl: '/partials/myartists.html' };

	    var productions = { controller: 'ProductionsCtrl', templateUrl: '/partials/productions/productions.html' };
	    var productionsPublicView = { controller: 'ProductionsPublicViewCtrl', templateUrl: '/partials/productions/productions-public-view.html' };
	    var production = { controller: 'ProductionBookCtrl', templateUrl: '/partials/productionBook/production-book.html' };
	    var completed = { controller: 'ProductionBookCompletedCtrl', templateUrl: '/partials/productionBook/production-book-completed.html' };
	    var assemble = { controller: 'AssembleCtrl', templateUrl: '/partials/productionBook/assemble.html' };
	    var schedule = { controller: 'ScheduleCtrl', templateUrl: '/partials/productionBook/schedule.html' };
	    var travel = { controller: 'TravelCtrl', templateUrl: '/partials/productionBook/travel.html' };
	    var attachment = { controller: 'AttachmentCtrl', templateUrl: '/partials/productionBook/attachment.html' };

	    var calendar = { controller: 'CalendarCtrl', templateUrl: '/partials/calender/calendar.html' };

	    var winkBoards = { controller: 'WinkBoardsCtrl', templateUrl: '/partials/winkBoard/winkboards.html' };
	    var winkBoard = { controller: 'WinkBoardCtrl', templateUrl: '/partials/winkBoard/winkboard.html' };
	    var winkBoardsPublicView = { controller: 'WinkBoardsPublicViewCtrl', templateUrl: '/partials/winkBoard/winkboards-public-view.html' };
	    var winkBoardPublicView = { controller: 'WinkBoardPublicViewCtrl', templateUrl: '/partials/winkBoard/winkboard-public-view.html' };

	    var story = { controller: 'StoryCtrl', templateUrl: '/partials/story/story.html' };
	    var behindScene = { controller: 'BehindSceneCtrl', templateUrl: '/partials/story/behindscene.html' };
	    var credits = { controller: 'CreditsCtrl', templateUrl: '/partials/story/credits.html' };

	    var account = { controller: 'AccountCtrl', templateUrl: '/partials/account/account.html' };
	    var billing = { controller: 'BillingCtrl', templateUrl: '/partials/account/billing.html' };

	    var talent = { controller: 'TalentCtrl', templateUrl: '/partials/profile/talent.html' };
	    var profileproductions = { controller: 'ProductionsCtrl', templateUrl: '/partials/profile/productions.html' };

	    var coupon = { controller: 'CouponCtrl', templateUrl: '/partials/admin/coupon.html' };
	    var loginHistory = { controller: 'LoginHistoryCtrl', templateUrl: '/partials/admin/loginHistory.html' };
	    var auditLog = { controller: 'AuditLogCtrl', templateUrl: '/partials/admin/auditLog.html' };
	    var company = { controller: 'CompanyCtrl', templateUrl: '/partials/admin/company.html' };
	    var sentLog = { controller: 'SentLogCtrl', templateUrl: '/partials/admin/sentLog.html' };
	    var user = { controller: 'UserCtrl', templateUrl: '/partials/admin/user.html' };
	    var productReleases = { controller: 'ProductReleasesCtrl', templateUrl: '/partials/admin/productReleases.html' };


	    $routeProvider
			.when('/', home)
            .when('/home', home)

            .when('/account', account)
            .when('/billing', billing)

            .when('/productions', productions)
            .when('/productions-public-view', productionsPublicView)
            .when('/production/current', production)
            .when('/production/completed', completed)
            .when('/production/:productionID/assemble', assemble)
            .when('/production/:productionID/schedule', schedule)
            .when('/production/:productionID/travel', travel)
            .when('/production/:productionID/attachments', attachment)

            .when('/calendar', calendar)

            .when('/notifications', notification)
            .when('/messages', messages)
            .when('/myartists', myartists)

            .when('/winkboards', winkBoards)
            .when('/winkboard/:winkDoardID', winkBoard)
            .when('/winkboards-public-view', winkBoardsPublicView)
            .when('/winkboard-public-view/:winkDoardID', winkBoardPublicView)

            .when('/story', story)
            .when('/behindscene', behindScene)
            .when('/credits', credits)

            .when('/profile/talent', talent)
            .when('/profile/productions', profileproductions)

            .when('/admin/coupon', coupon)
            .when('/admin/history', loginHistory)
            .when('/admin/audit', auditLog)
            .when('/admin/company', company)
            .when('/admin/sent', sentLog)
            .when('/admin/user', user)
            .when('/admin/product', productReleases)


			.otherwise({
			    redirectTo: '/'
			});
	});

require('headerCtrl');
require("lenspire-directives");
require("common-service");

require('homeCtrl');
require('notificationCtrl');
require('messagesCtrl');
require('newMessageCtrl');
require('myartistsCtrl');

require('profile/talentCtrl');
require('profile/talentSelectCtrl');

require('productions/productionsCtrl');
require('productions/productionsPublicViewCtrl');
require('productionBook/productionBookCtrl');
require('productionBook/productionBookCompletedCtrl');
require('productionBook/productionBookCreationCtrl');
require('productionBook/assembleCtrl');
require('productionBook/crewCreationCtrl');
require('productionBook/scheduleCtrl');
require('productionBook/selectCrewCtrl');
require('productionBook/addCrewCtrl');
require('productionBook/travelCtrl');
require('productionBook/itineraryCreationCtrl');
require('productionBook/productionLogoUploadCtrl');
require('productionBook/productionInviteCollaboratorsCtrl');
require('productionBook/attachmentCtrl');
require('productionBook/sharedWinkboardCtrl');
require('productionBook/productionEditCtrl');

require('account/accountCtrl');
require('account/billingCtrl');

require('calendar/calendarCtrl');

require('winkBoard/winkBoardsCtrl');
require('winkBoard/winkBoardCtrl');
require('winkBoard/winkBoardsPublicViewCtrl');
require('winkBoard/winkBoardPublicViewCtrl');
require('winkBoard/winkBoardAddCtrl')
require('winkBoard/winkBoardCreationCtrl');

require('story/storyCtrl');
require('story/behindsceneCtrl');
require('story/creditsCtrl');
require('story/enlargedCtrl')

require('layout/profileCtrl');
require('layout/profileAgentCtrl');

require('admin/couponCtrl');
require('admin/generateCouponCtrl');
require('admin/loginHistoryCtrl');
require('admin/auditLogCtrl');
require('admin/companyCtrl'); 
require('admin/addCompanyCtrl');
require('admin/sentLogCtrl');
require('admin/userCtrl');
require('admin/productReleasesCtrl');



