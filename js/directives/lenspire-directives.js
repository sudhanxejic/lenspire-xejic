﻿
angular.module("uib/template/datepicker/day.html", []).run(["$templateCache", function (t) { t.put("uib/template/datepicker/day.html", '<table   id="datepicker"  class="uib-daypicker" role="grid" aria-labelledby="{{::uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n<thead>\n<tr>\n<th colspan="2"><button md-prevent-menu-close type="button" class="btn btn-default back btn-sm pull-left uib-left " ng-click="move(-1)" tabindex="-1"><md-icon class="icon icon-datepicker-left color-azure"></md-icon></button></th>\n <th colspan="3"><button md-prevent-menu-close id="{{::uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn displayMonth btn-default btn-sm uib-title" ng-click="toggleMode()" ng-disabled="datepickerMode === maxMode" tabindex="-1"><strong class="displayMonthStyle">{{title}}</strong></button></th>\n<th colspan="2" ><button type="button" md-prevent-menu-close class="btn btn-default nextMonth btn-sm pull-right uib-right" ng-click="move(1)" tabindex="-1"><md-icon class="icon icon-datepicker-right color-azure"></md-icon></button></th>\n</tr>\n<tr>\n<th ng-repeat="label in ::labels track by $index" class="text-center  day-text"><small class="dayStyle" aria-label="{{::label.full}}">{{::label.abbr}}</small></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr class="uib-weeks "  ng-repeat="row in rows track by $index">\n  <td ng-repeat="dt in row" class="uib-day displayDate " role="gridcell"\n id="{{::dt.uid}}"\n ng-class="::dt.customClass">\n<button type="button" class="btn dateOnMonth btn-default btn-sm"\n uib-is-class="\n \'btn-info\' for selectedDt,\n \'active\' for activeDt\n  on dt"\n ng-click="select(dt.date)"\n  ng-disabled="::dt.disabled"\n tabindex="-1"><span class="display" ng-class="::{\'text-muted\': dt.secondary, \'text-info\': dt.current}">{{::dt.label}}</span></button>\n</td>\n</tr>\n</tbody>\n</table>\n') }]);

angular.module("uib/template/datepicker/month.html", []).run(["$templateCache", function (a) { a.put("uib/template/datepicker/month.html", '<table  id="datepicker" class="uib-monthpicker" role="grid" aria-labelledby="{{::uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n<thead>\n<tr>\n<th><button md-prevent-menu-close type="button" class="btn btn-default back btn-sm pull-left uib-left" ng-click="move(-1)" tabindex="-1"><md-icon class="icon icon-datepicker-left color-azure"></md-icon></button></th>\n<th><button md-prevent-menu-close id="{{::uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn displayMonth btn-default btn-sm uib-title" ng-click="toggleMode()" ng-disabled="datepickerMode === maxMode" tabindex="-1"><strong class="displayMonthStyle">{{title}}</strong></button></th>\n<th><button md-prevent-menu-close type="button" class="btn btn-default nextMonth btn-sm pull-right uib-right" ng-click="move(1)" tabindex="-1"><md-icon class="icon icon-datepicker-right color-azure"></md-icon></button></th>\n</tr>\n</thead>\n<tbody>\n<tr class="uib-months rowheightyear" ng-repeat="row in rows track by $index">\n<td ng-repeat="dt in row" class="uib-month  text-center" role="gridcell"\n id="{{::dt.uid}}"\n ng-class="::dt.customClass">\n<button md-prevent-menu-close type="button" class="btn buttonheightmonth radius  btn-default"\n uib-is-class="\n \'btn-info\' for selectedDt,\n \'active\' for activeDt\n on dt"\n ng-click="select(dt.date)"\n ng-disabled="::dt.disabled"\n tabindex="-1"><span ng-class="::{\'text-info\': dt.current}">{{::dt.label}}</span></button>\n</td>\n</tr>\n  </tbody>\n</table>\n') }]);

angular.module("uib/template/datepicker/year.html", []).run(["$templateCache", function (a) { a.put("uib/template/datepicker/year.html", '<table id="datepicker" class="uib-yearpicker" role="grid" aria-labelledby="{{::uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n<thead>\n<tr>\n<th><button md-prevent-menu-close type="button" class="btn btn-default back btn-sm pull-left uib-left" ng-click="move(-1)" tabindex="-1"><md-icon class="icon icon-datepicker-left color-azure"></md-icon></button></th>\n<th colspan="{{::columns - 2}}"><button md-prevent-menu-close id="{{::uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn displayMonth  btn-default btn-sm uib-title" ng-click="toggleMode()" ng-disabled="datepickerMode === maxMode" tabindex="-1"><strong>{{title}}</strong></button></th>\n<th><button md-prevent-menu-close type="button" class="btn nextMonth btn-default btn-sm pull-right uib-right" ng-click="move(1)" tabindex="-1"><md-icon class="icon icon-datepicker-right color-azure"></md-icon></button></th>\n</tr>\n</thead>\n<tbody>\n<tr class="uib-years rowheightyear" ng-repeat="row in rows track by $index">\n<td ng-repeat="dt in row" class="uib-year text-center" role="gridcell"\n id="{{::dt.uid}}"\n ng-class="::dt.customClass">\n<button md-prevent-menu-close type="button" class="btn btn-default buttonheight"\n uib-is-class="\n \'btn-info\' for selectedDt,\n \'active\' for activeDt\n on dt"\n ng-click="select(dt.date)"\n ng-disabled="::dt.disabled"\n tabindex="-1"><span ng-class="::{\'text-info\': dt.current}">{{::dt.label}}</span></button>\n</td>\n</tr>\n  </tbody>\n</table>\n') }]);

angular.module("uib/template/timepicker/timepicker.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("uib/template/timepicker/timepicker.html",
      "<table class=\"uib-timepicker\">\n" +
      "  <tbody>\n" +
      "    <tr class=\"text-center\" ng-show=\"::showSpinners\">\n" +
      "      <td class=\"uib-increment hours\"><a ng-click=\"incrementHours()\" md-prevent-menu-close ng-class=\"{disabled: noIncrementHours()}\" class=\"btn btn-link\" ng-disabled=\"noIncrementHours()\" tabindex=\"{{::tabindex}}\"><span class=\"icon icon-datepicker-left rotate-90\"></span></a></td>\n" +
      "      <td>&nbsp;</td>\n" +
      "      <td class=\"uib-increment minutes\"><a ng-click=\"incrementMinutes()\" md-prevent-menu-close ng-class=\"{disabled: noIncrementMinutes()}\" class=\"btn btn-link\" ng-disabled=\"noIncrementMinutes()\" tabindex=\"{{::tabindex}}\"><span class=\"icon icon-datepicker-left rotate-90\"></span></a></td>\n" +
      "      <td ng-show=\"showSeconds\">&nbsp;</td>\n" +
      "      <td ng-show=\"showSeconds\" class=\"uib-increment seconds\"><a ng-click=\"incrementSeconds()\" ng-class=\"{disabled: noIncrementSeconds()}\" class=\"btn btn-link\" ng-disabled=\"noIncrementSeconds()\" tabindex=\"{{::tabindex}}\"><span class=\"glyphicon glyphicon-chevron-up\"></span></a></td>\n" +
      "      <td ng-show=\"showMeridian\"></td>\n" +
      "    </tr>\n" +
      "    <tr>\n" +
      "      <td class=\"form-group uib-time hours\" ng-class=\"{'has-error': invalidHours}\">\n" +
      "        <input type=\"text\" placeholder=\"HH\" ng-model=\"hours\" ng-change=\"updateHours()\" class=\"form-control text-center\" ng-readonly=\"::readonlyInput\" maxlength=\"2\" tabindex=\"{{::tabindex}}\" ng-disabled=\"noIncrementHours()\" ng-blur=\"blur()\">\n" +
      "      </td>\n" +
      "      <td class=\"uib-separator\">:</td>\n" +
      "      <td class=\"form-group uib-time minutes\" ng-class=\"{'has-error': invalidMinutes}\">\n" +
      "        <input type=\"text\" placeholder=\"MM\" ng-model=\"minutes\" ng-change=\"updateMinutes()\" class=\"form-control text-center\" ng-readonly=\"::readonlyInput\" maxlength=\"2\" tabindex=\"{{::tabindex}}\" ng-disabled=\"noIncrementMinutes()\" ng-blur=\"blur()\">\n" +
      "      </td>\n" +
      "      <td ng-show=\"showSeconds\" class=\"uib-separator\">:</td>\n" +
      "      <td class=\"form-group uib-time seconds\" ng-class=\"{'has-error': invalidSeconds}\" ng-show=\"showSeconds\">\n" +
      "        <input type=\"text\" placeholder=\"SS\" ng-model=\"seconds\" ng-change=\"updateSeconds()\" class=\"form-control text-center\" ng-readonly=\"readonlyInput\" maxlength=\"2\" tabindex=\"{{::tabindex}}\" ng-disabled=\"noIncrementSeconds()\" ng-blur=\"blur()\">\n" +
      "      </td>\n" +
      "      <td ng-show=\"showMeridian\" class=\"uib-time am-pm\"><button type=\"button\" md-prevent-menu-close ng-class=\"{disabled: noToggleMeridian()}\" class=\"btn btn-default text-center\" ng-click=\"toggleMeridian()\" ng-disabled=\"noToggleMeridian()\" tabindex=\"{{::tabindex}}\">{{meridian}}</button></td>\n" +
      "    </tr>\n" +
      "    <tr class=\"text-center\" ng-show=\"::showSpinners\">\n" +
      "      <td class=\"uib-decrement hours\"><a ng-click=\"decrementHours()\" md-prevent-menu-close ng-class=\"{disabled: noDecrementHours()}\" class=\"btn btn-link\" ng-disabled=\"noDecrementHours()\" tabindex=\"{{::tabindex}}\"><span class=\"icon icon-datepicker-right rotate-90\"></span></a></td>\n" +
      "      <td>&nbsp;</td>\n" +
      "      <td class=\"uib-decrement minutes\"><a ng-click=\"decrementMinutes()\" md-prevent-menu-close ng-class=\"{disabled: noDecrementMinutes()}\" class=\"btn btn-link\" ng-disabled=\"noDecrementMinutes()\" tabindex=\"{{::tabindex}}\"><span class=\"icon icon-datepicker-right rotate-90\"></span></a></td>\n" +
      "      <td ng-show=\"showSeconds\">&nbsp;</td>\n" +
      "      <td ng-show=\"showSeconds\" class=\"uib-decrement seconds\"><a ng-click=\"decrementSeconds()\" ng-class=\"{disabled: noDecrementSeconds()}\" class=\"btn btn-link\" ng-disabled=\"noDecrementSeconds()\" tabindex=\"{{::tabindex}}\"><span class=\"glyphicon glyphicon-chevron-down\"></span></a></td>\n" +
      "      <td ng-show=\"showMeridian\"></td>\n" +
      "    </tr>\n" +
      "  </tbody>\n" +
      "</table>\n" +
      "");
}]);
angular.module('lenspireApp').directive('ngHtmlCompile', ["$compile", function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.ngHtmlCompile, function (newValue, oldValue) {
                element.html(newValue);
                $compile(element.contents())(scope);
            });
        }
    }
}]);

angular.module('lenspireApp').directive('xeHref', ['$location', function ($location) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.on('click', function () {
                $location.path(attr.xeHref)
                scope.$apply();
            });
        }
    }
}]);

