angular.module('lenspireApp')
	.controller('WinkBoardCtrl', function WinkBoardCtrl($scope, $mdMedia, $window, $mdDialog, $mdSidenav) {
	    'use strict';
	    $scope.currentNavItem = "winkborad";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.profileTemplate = "/partials/layout/profile.html";

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen) {
                      $scope.sidenavOpen = false;
                  }
              }
             );

	    $scope.winkBoard = {
	        id: 1,
	        name: "Spring Fashion",
            type: "public",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        collaborators: [{ avatar: "images/data/avatar-1.png", job: "OWNER", name: "Dalia Kenwood" },
            { avatar: "images/data/avatar-2.png", job: null, name: "Ellis Hanson" },
	        { avatar: "images/data/avatar-3.png", job: null, name: "Nicholas George" },
	        { avatar: "images/data/avatar-4.png", job: null, name: "Jack Harrison" },
	        { avatar: "images/data/avatar-5.png", job: null, name: "Kathleen Banks" }],
	        feeds: [{
	            image: "images/data/photo-1.png",
	            publishers: [{
	                publisherName: "Cosmopolitan",
	                publisherAvatar: "images/data/avatar-1.png"
	            }],
	            winkName: "Fall Collection",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-2.png",
	            publishers: [{
	                publisherName: "National Geographics",
	                publisherAvatar: "images/data/avatar-3.png"
	            }],
	            winkName: "Violet River",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-3.png",
	            publishers: [{
	                publisherName: "People",
	                publisherAvatar: "images/data/avatar-6.png"
	            }],
	            winkName: "Lost in Nature",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-4.png",
	            publishers: [{
	                publisherName: "Complex",
	                publisherAvatar: "images/data/avatar-10.png"
	            }],
	            winkName: "The Fighter",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-5.png",
	            publishers: [{
	                publisherName: "Vanity Fair",
	                publisherAvatar: "images/data/avatar-12.png"
	            }],
	            winkName: "Summer Body",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-6.png",
	            publishers: [{
	                publisherName: "Billboard",
	                publisherAvatar: "images/data/avatar-13.png"
	            }],
	            winkName: "Plaza Water",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-1.png",
	            publishers: [{
	                publisherName: "Cosmopolitan",
	                publisherAvatar: "images/data/avatar-1.png"
	            }],
	            winkName: "Fall Collection",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-2.png",
	            publishers: [{
	                publisherName: "National Geographics",
	                publisherAvatar: "images/data/avatar-3.png"
	            }],
	            winkName: "Violet River",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-3.png",
	            publishers: [{
	                publisherName: "People",
	                publisherAvatar: "images/data/avatar-6.png"
	            }],
	            winkName: "Lost in Nature",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-4.png",
	            publishers: [{
	                publisherName: "Complex",
	                publisherAvatar: "images/data/avatar-10.png"
	            }],
	            winkName: "The Fighter",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-5.png",
	            publishers: [{
	                publisherName: "Vanity Fair",
	                publisherAvatar: "images/data/avatar-12.png"
	            }],
	            winkName: "Summer Body",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-6.png",
	            publishers: [{
	                publisherName: "Billboard",
	                publisherAvatar: "images/data/avatar-13.png"
	            }],
	            winkName: "Plaza Water",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-4.png",
	            publishers: [{
	                publisherName: "Complex",
	                publisherAvatar: "images/data/avatar-10.png"
	            }],
	            winkName: "The Fighter",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-5.png",
	            publishers: [{
	                publisherName: "Vanity Fair",
	                publisherAvatar: "images/data/avatar-12.png"
	            }],
	            winkName: "Summer Body",
	            active: false,
	            public: true
	        }, {
	            image: "images/data/photo-6.png",
	            publishers: [{
	                publisherName: "Billboard",
	                publisherAvatar: "images/data/avatar-13.png"
	            }],
	            winkName: "Plaza Water",
	            active: false,
	            public: true
	        }]

	    }

	    $scope.openEnlarged = function (ev, feed) {
	        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
	        $mdDialog.show({
	            controller: 'EnlargedCtrl',
	            templateUrl: '/partials/story/enlarged.html',
	            parent: angular.element('body'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	            resolve: {
	                feed: function () {
	                    return feed;
	                }
	            }
	        })
            .then(function (response) {

            }, function () {

            });
	    }

	    $scope.eidtWinkboard = function (ev, _type) {
	        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
	        $mdDialog.show({
	            controller: 'WinkBoardCreationCtrl',
	            templateUrl: '/partials/winkBoard/winkboard-creation.html',
	            parent: angular.element('body'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: useFullScreen,
	            resolve: {
	                winkboard: function () {
	                    return {
	                        wink: angular.copy($scope.winkBoard),
	                        type: _type
	                    }
	                }
	            }
	        })
            .then(function (response) {
                if (response)
                    $scope.winkBoard = response;
            }, function () {

            });
	    };


	    $scope.deleteFeed = function (index) {
	        $scope.winkBoard.feeds.splice(index, 1);
	    }
	});
