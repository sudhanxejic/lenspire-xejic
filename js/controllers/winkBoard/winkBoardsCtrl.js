angular.module('lenspireApp')
	.controller('WinkBoardsCtrl', function WinkBoardsCtrl($scope, $mdSidenav, $window, $mdMedia, $mdDialog) {
	    'use strict';
	    $scope.currentNavItem = "winkborad";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.profileTemplate = "/partials/layout/profile.html";

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen == true) {
                      $scope.sidenavOpen = false;
                  }
              }
             );


	    $scope.publicWinkBoards = [{
	        name: "Spring Fashion",
	        images: ["images/data/photo-9.png", "images/data/photo-3.png", "images/data/photo-6.png", "images/data/photo-4.png", "images/data/photo-5.png", "images/data/photo-1.png"]
	    }, {
	        name: "Shooting Locations",
	        images: ["images/data/photo-2.png", "images/data/photo-11.png"]
	    }, {
	        name: "Concepts",
	        images: ["images/data/photo-8.png"]
	    }];

	    $scope.privateWinkBoards = [{
	        name: "Spring Fashion",
	        images: ["images/data/photo-9.png", "images/data/photo-3.png", "images/data/photo-6.png", "images/data/photo-4.png", "images/data/photo-5.png", "images/data/photo-1.png"]
	    }];


	    $scope.addWinkboard = function (ev, _type) {
	        $mdDialog.show({
	            controller: 'WinkBoardCreationCtrl',
	            templateUrl: '/partials/winkBoard/winkboard-creation.html',
	            parent: angular.element('body'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true                                                                                                                                                                                                                ,
	            resolve: {
	                winkboard: function () {
	                    return {
	                        wink: null,
	                        type: _type
	                    }
	                }
	            }
	        })
            .then(function (response) {
                if (response) {
                    if (_type == 'public')
                        $scope.publicWinkBoards.unshift(response);
                    else
                        $scope.privateWinkBoards.unshift(response)
                }
            }, function () {

            });
	    }

	});
