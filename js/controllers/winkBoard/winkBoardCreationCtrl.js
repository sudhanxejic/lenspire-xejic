angular.module('lenspireApp')
	.controller('WinkBoardCreationCtrl', function WinkBoardCreationCtrl($scope, $mdDialog, winkboard) {
	    'use strict';

	    $scope.close = function () {
	        $mdDialog.cancel();
	    };

	    $scope.save = function () {
	        if ($scope.winkboard.name)
	            $mdDialog.hide($scope.winkboard);
	    };

	    $scope.winkboard = winkboard.wink;
	    if (!$scope.winkboard) {
	        $scope.winkboard = {
	            type: winkboard.type
	        };
	        $scope.winkboard.collaborators = [{ avatar: "images/data/avatar-1.png", job: "OWNER", name: "Dalia Kenwood" }];
	    }

	    $scope.hoverIn = function () {
	        this.removebtn = true;
	    };

	    $scope.hoverOut = function () {
	        this.removebtn = false;
	    };
	});
