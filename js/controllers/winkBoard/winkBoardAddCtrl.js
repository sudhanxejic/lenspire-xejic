angular.module('lenspireApp')
	.controller('WinkBoardAddCtrl', function WinkBoardAddCtrl($scope, $mdDialog, feed) {
	    $scope.feed = feed;
	    $scope.winking = [{ avatar: "images/data/avatar-1.png", job: "Art Director", name: "Andre Lewis" },
        { avatar: "images/data/avatar-2.png", job: "Photographer", name: "Dalia Kenwood" },
        { avatar: "images/data/avatar-3.png", job: "Make Up Artist", name: "Jesse Keller" },
        { avatar: "images/data/avatar-4.png", job: "Model", name: "Jessica Nguyen" },
        { avatar: "images/data/avatar-5.png", job: "Set Designer", name: "Ben Washington" },
        { avatar: "images/data/avatar-6.png", job: "Fashion", name: "Jane Wu" }];

	    $scope.winkboard = [{
	        name: "Summer Fashion",
	        images: ["images/data/photo-9.png", "images/data/photo-3.png", "images/data/photo-6.png", "images/data/photo-4.png", "images/data/photo-5.png", "images/data/photo-1.png"]
	    }, {
	        name: "",
	        images: [],
	    }];

	    $scope.addWinkboard = function () {
	        $scope.winkboard.push({
	            name: "",
	            images: [],
	        })
	    };

	    $scope.winkSelected = function (wink) {
	        $.each($scope.winkboard, function (index, val) {
	            val.selected = false;
	        });
	        wink.selected = true;
	    }

	    $scope.close = function () {
	        $mdDialog.hide();
	    };
	});
