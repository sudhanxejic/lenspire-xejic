angular.module('lenspireApp')
	.controller('ProductionsPublicViewCtrl', function PortfoliosCtrl($scope, $window, $mdSidenav, $mdMedia, $mdDialog) {
	    'use strict';
	    $scope.currentNavItem = "productions";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.profileTemplate = "/partials/layout/profile-public-view.html";

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen == true) {
                      $scope.sidenavOpen = false;
                  }
              }
             );

	    $scope.productions = [{
	        id: 1,
	        name: "Women's Fashion",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-big.png",
	        hidden: false
	    }, {
	        id: 2,
	        name: "Life Style",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-1.png",
	        hidden: true
	    }, {
	        id: 3,
	        name: "Men's Sports",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-3.png",
	        hidden: false
	    }, {
	        id: 4,
	        name: "Fashion",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-6.png",
	        hidden: false
	    }, {
	        id: 5,
	        name: "Women's Fashion",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-big.png",
	        hidden: false
	    }, {
	        id: 6,
	        name: "Life Style",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-1.png",
	        hidden: true
	    }, {
	        id: 7,
	        name: "Men's Sports",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-3.png",
	        hidden: false
	    }, {
	        id: 8,
	        name: "Fashion",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim.",
	        tags: " Art Direction, Fashion, Men, Women",
	        image: "images/data/photo-6.png",
	        hidden: false
	    }];

	    $scope.hidden = function (prod) {
	        prod.hidden = !prod.hidden;
	    }

	});
