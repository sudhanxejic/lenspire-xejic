angular.module('lenspireApp')
	.controller('HomeCtrl', function HomeCtrl($scope, $mdSidenav, $mdMedia, $mdDialog, CommonService) {
	    'use strict';
	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("right").toggle()
	    }
	    $scope.$watch(function () { return $mdMedia('xs') },
             function (screenSize) {
                 $scope.screen = screenSize;
                 if (screenSize) {
                     $scope.sidenavOpen = false;
                 }
             }
            );
	    $scope.user = CommonService.getUser();
	    if ($scope.user.agent)
	        $scope.sideBar = "/partials/layout/agent-sidebar.html";
	    else
	        $scope.sideBar = "/partials/layout/artist-sidebar.html";

	    $scope.openEnlarged = function (ev, feed) {
	        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
	        $mdDialog.show({
	            controller: 'EnlargedCtrl',
	            templateUrl: '/partials/story/enlarged.html',
	            parent: angular.element('body'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	            resolve: {
	                feed: function () {
	                    return feed;
	                }
	            }
	        })
            .then(function (response) {

            }, function () {

            });
	    }

	    $scope.addToWinkboard = function (ev, feed) {
	        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
	        $mdDialog.show({
	            controller: 'WinkBoardAddCtrl',
	            templateUrl: '/partials/winkBoard/winkboard-add.html',
	            parent: angular.element('body'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	            resolve: {
	                feed: function () {
	                    return feed;
	                }
	            }
	        })
            .then(function (response) {

            }, function () {

            });
	    }

	    $scope.feeds = [{
	        image: "images/data/photo-1.png",
	        publishers: [{
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-2.png"
	        }],
	        winkName: "Fall Collection",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-2.png",
	        publishers: [{
	            publisherName: "Geographics",
	            publisherAvatar: "images/data/avatar-3.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-4.png"
	        }, {
	            publisherName: "People",
	            publisherAvatar: "images/data/avatar-5.png"
	        }],
	        winkName: "Violet River",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-3.png",
	        publishers: [{
	            publisherName: "People",
	            publisherAvatar: "images/data/avatar-6.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-7.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-8.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-9.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-10.png"
	        }],
	        winkName: "Lost in Nature",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-4.png",
	        publishers: [{
	            publisherName: "Complex",
	            publisherAvatar: "images/data/avatar-10.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-11.png"
	        }],
	        winkName: "The Fighter",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-5.png",
	        publishers: [{
	            publisherName: "Vanity Fair",
	            publisherAvatar: "images/data/avatar-12.png"
	        }],
	        winkName: "Summer Body",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-6.png",
	        publishers: [{
	            publisherName: "Billboard",
	            publisherAvatar: "images/data/avatar-13.png"
	        }],
	        winkName: "Plaza Water",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-7.png",
	        publishers: [{
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-2.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }],
	        winkName: "Fall Collection",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-8.png",
	        publishers: [{
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }],
	        winkName: "Violet River",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-9.png",
	        publishers: [{
	            publisherName: "People",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }],
	        winkName: "Lost in Nature",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-5.png",
	        publishers: [{
	            publisherName: "Vanity Fair",
	            publisherAvatar: "images/data/avatar-12.png"
	        }],
	        winkName: "Summer Body",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-6.png",
	        publishers: [{
	            publisherName: "Billboard",
	            publisherAvatar: "images/data/avatar-13.png"
	        }],
	        winkName: "Plaza Water",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-7.png",
	        publishers: [{
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-2.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }],
	        winkName: "Fall Collection",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-8.png",
	        publishers: [{
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }],
	        winkName: "Violet River",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-9.png",
	        publishers: [{
	            publisherName: "People",
	            publisherAvatar: "images/data/avatar-13.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }],
	        winkName: "Lost in Nature",
	        active: false,
	        public: true
	    }];
	});
