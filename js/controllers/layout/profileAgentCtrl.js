angular.module('lenspireApp')
	.controller('ProfileAgentCtrl', function ProfileAgentCtrl($scope) {

	    this.profile = {
	        avatar: "images/data/company-logo.png",
	        name: "BRYNN CREATIVES",
	        followersCount: 11,
	        roles: ["Full Service Production"],
	        aboutme: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	        address: "1500 Marilla St, \n Dallas, TX 75201",
	        contact: "Website: www.brynncreative.com \n Email: info@brynncreative.com \n Phone: 214.673.5496 \n Fax: 214.594.8060"       
	    }
	});
