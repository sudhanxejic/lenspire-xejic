angular.module('lenspireApp')
	.controller('ProfileCtrl', function ProfileCtrl($scope) {

	    this.profile = {
	        avatar: "images/data/artists-avatar.png",
	        name: "STEVE ROGERS",
	        followersCount: 128,
	        projectsCount: 23,
	        roles: ["Photographer", "Art Director"],
	        specialty: ["Fashion Photography"],
	        aboutme: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris a dui molestie, porttitor sapien quis, venenatis turpis. Nunc auctor dui neque, varius sodal. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	        agency: ["BRYNN CREATIVE"],
	        agencyContact: "Brynn Isom \n steverogers@email.com \n (123) 456 7890",
	        personalContact: "instagram.com/daliakenwood",
	        address: "1500 Marilla St, \n Dallas, TX 75201",
	        contact: "Website: www.brynncreative.com \n Email: info@brynncreative.com \n Phone: 214.673.5496 \n Fax: 214.594.8060",
	        showNoOfFollowers: true,
	        showNoOfProjects: true,
	        showAgency: true,
	        showAgencyContact: true,
	        showPersonalWebsite: true
	    }

	});
