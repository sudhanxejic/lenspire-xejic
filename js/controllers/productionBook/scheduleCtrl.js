﻿angular.module('lenspireApp')
	.controller('ScheduleCtrl', function ScheduleCtrl($scope, $mdSidenav, $window, $mdDialog, $routeParams, CommonService, $mdMedia) {
	    'use strict';
	    $scope.sidebar = "/partials/productionBook/production-sidebar.html";
	    $scope.currentNavItem = "schedule";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = "#/production/" + $scope.book.id + "/" + navItem;
	    }
	    $scope.book = CommonService.getProductinBook($routeParams.productionID);
	    $scope.today = new Date();
	    $scope.schedules = [];
	    $scope.numberOfCrews = 2;
	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen == true) {
                      $scope.sidenavOpen = false;
                      $scope.numberOfCrews = 1;
                  } else {
                      $scope.numberOfCrews = 2;
                  }
              }
             );

	    $scope.addSchedule = function (time) {
	        $scope.schedules.push({
	            date: time,
	            event: []
	        });
	    }

	    $scope.addcrew = function (schedule) {
	        schedule.event.push({
	            time: new Date(),
	            location: null,
	            action: null,
	            crew: []
	        });
	    }

	    $scope.addcrewdialog = function (ev,schedule) {
	        $mdDialog.show({
	            controller: "AddCrewCtrl",
	            templateUrl: '/partials/productionBook/add-crew.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	        }).then(function (crews) {
	            angular.forEach(crews, function (val) {
	                schedule.crew.push(val);
	            });
	        });
	    };

	    $scope.remove = function (idx) {
	        $scope.schedules.splice(idx, 1);
	    }

	    $scope.deleteEvent = function (event, idx) {
	        event.splice(idx, 1);
	    }

	    $scope.removeExtra = function (idx,value) {
	        value.splice(idx, 1);
	    }

	});