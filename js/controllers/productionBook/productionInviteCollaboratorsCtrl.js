﻿angular.module('lenspireApp')
	.controller('ProductionInviteCollaboratorsCtrl', function ProductionInviteCollaboratorsCtrl($scope, $mdMedia, $mdDialog, CommonService) {
	    'use strict';

	    $scope.hide = function () {
	        $mdDialog.hide();
	    };

	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };

	    $scope.removecollaborator = function (index) {
	        $scope.collaborators.splice(index, 1);
	    }

	    $scope.collaborators = [{
	        avatar:'images/data/avatar-13.png',
	        profession: 'Owner',
	        name: 'Dalia Kenwood'
	    }, {
	        avatar: 'images/data/avatar-14.png',
	        profession: 'CAN EDIT',
	        name: 'Ellis Hanson'
	    }, {
	        avatar: 'images/data/avatar-12.png',
	        profession: 'VIEW ONLY',
	        name: 'Nicholas George'
	    }, {
	        avatar: 'images/data/avatar-3.png',
	        profession: 'VIEW ONLY',
	        name: 'Jack Harrison'
	    }, {
	        avatar: 'images/data/avatar-5.png',
	        profession: 'VIEW ONLY',
	        name: 'Kathleen Banks'
	    }];

	});