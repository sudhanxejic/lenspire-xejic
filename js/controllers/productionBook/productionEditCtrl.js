﻿angular.module('lenspireApp')
	.controller('ProductionEditCtrl', function ProductionEditCtrl($filter, $scope, $mdSidenav, $mdDialog, $mdMedia, $routeParams, CommonService) {
	    'use strict';
	    var self = this;
	    this.book = CommonService.getProductinBook($routeParams.productionID);
       
	   this.updatelogo = function (ev) {
	        $mdDialog.show({
	            controller: "ProductionLogoUploadCtrl",
	            templateUrl: '/partials/productionBook/production-logo-upload.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	            locals: {
	                book: angular.copy(self.book)
	            }
	        })
	   };

	   this.editProductinBook = function (ev) {
	       $mdDialog.show({
	           controller: "ProductionBookCreationCtrl",
	           templateUrl: '/partials/productionBook/production-book-creation.html',
	           parent: angular.element(document.body),
	           targetEvent: ev,
	           clickOutsideToClose: true,
	           fullscreen: true,
	           locals: {
	               book: self.book
	           }
	       }).then(function () {
	           $scope.productinBooks = CommonService.getProductinBooks();
	       });
	   };

	   this.inviteCollaborators = function (ev) {
	        $mdDialog.show({
	            controller: "ProductionInviteCollaboratorsCtrl",
	            templateUrl: '/partials/productionBook/production-invite-collaborators.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,

	        });
	   };

	   $scope.$watch(function () {
	       //CommonService.setProductinBook(self.book);
	   });

	   this.addClient = function () {
	       this.book.client.push(angular.copy(this.newClient));
	       this.newClient = null;
	   }

	   this.removeClient = function (index) {
	       this.book.client.splice(index, 1);
	   }

	   this.addAgency = function () {
	       this.book.agency.push(angular.copy(this.newAgency));
	       this.newAgency = null;
	   }

	   this.removeAgency = function (index) {
	       this.book.agency.splice(index, 1);
	   }

	});