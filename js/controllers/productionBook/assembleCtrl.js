﻿angular.module('lenspireApp')
	.controller('AssembleCtrl', function AssembleCtrl($filter, $scope, $window, $mdSidenav, $mdDialog, $mdMedia, $routeParams, CommonService) {
	    'use strict';
	    $scope.sidebar = "/partials/productionBook/production-sidebar.html";
	    var originatorEv;
	    $scope.hide = false;
	    $scope.selectAll = true;
	    $scope.onlySelected = false;
	    $scope.onlyChoice = false;
	    $scope.onlyAccept = false;
	    $scope.onlyUndo = false;
	    $scope.newArtist = false;

	    $scope.currentNavItem = "assemble";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = "#/production/" + $scope.book.id + "/" + navItem;
	    }

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen == true) {
                      $scope.sidenavOpen = false;
                  }
              }
             );

	    $scope.show = false;
	    $scope.commonService = CommonService;
	    $scope.book = CommonService.getProductinBook($routeParams.productionID);
	    $scope.book.selectAll = false;
	    $scope.book.assembles = [];
	    $scope.templates = [{
	        id: 1,
	        name: "Template 1"
	    }, {
	        id: 2,
	        name: "Template 2"
	    }, {
	        id: 3,
	        name: "Template 3"
	    }];

	    $scope.defaultMembers = [{
	        name: 'Chad Cooper',
	        avatar: 'images/data/avatar-10.png',
	        phoneNumber: "(123) 456 7890",
	        job: 'Art Director',
	        agent: {
	            name: 'Amy Agent',
	            agency: 'BRYNN CREATIVE',
	            avatar: 'images/data/avatar-6.png',
	            phoneNumber: "(123) 456 7890",
	        },
	        agents: [
                {
                    name: 'Amy Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-11.png',
                    phoneNumber: "(123) 456 7890",
                },
                {
                    name: 'Adam Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-12.png',
                    phoneNumber: "(123) 456 7890",
                },
                {
                    name: 'Brad Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-13.png',
                    phoneNumber: "(123) 456 7890",
                }]
	    }, {
	        name: 'Heather Greene',
	        avatar: 'images/data/avatar-7.png',
	        phoneNumber: "(123) 456 7890",
	        job: 'Art Director',
	        agent: {
	            name: 'Amy Agent',
	            agency: 'BRYNN CREATIVE',
	            avatar: 'images/data/avatar-11.png',
	            phoneNumber: "(123) 456 7890",
	        },
	        agents: [
                {
                    name: 'Amy Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-11.png',
                    phoneNumber: "(123) 456 7890",
                },
                {
                    name: 'Adam Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-12.png',
                    phoneNumber: "(123) 456 7890",
                },
                {
                    name: 'Brad Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-13.png',
                    phoneNumber: "(123) 456 7890",
                }]
	    }, {
	        name: 'Matt Jackson',
	        avatar: 'images/data/avatar-8.png',
	        phoneNumber: "(123) 456 7890",
	        job: 'Art Director',
	        agent: {
	            name: 'Amy Agent',
	            agency: 'BRYNN CREATIVE',
	            avatar: 'images/data/avatar-11.png',
	            phoneNumber: "(123) 456 7890",
	        },
	        agents: [
                {
                    name: 'Amy Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-11.png',
                    phoneNumber: "(123) 456 7890",
                },
                {
                    name: 'Adam Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-12.png',
                    phoneNumber: "(123) 456 7890",
                },
                {
                    name: 'Brad Agent',
                    agency: 'BRYNN CREATIVE',
                    avatar: 'images/data/avatar-13.png',
                    phoneNumber: "(123) 456 7890",
                }]
	    }];

	    $scope.removetemplate = function (index) {
	        $scope.templates.splice(index, 1);
	    }

	    $scope.newmember = function () {
	        $scope.newArtist = !$scope.newArtist;
	    }

	    $scope.newAgent = function () {
	        $scope.newAgentVal = !$scope.newAgentVal;
	    }

	    $scope.selectAllChange = function () {
	        $.each($scope.book.assembles, function (assemble_ind, assemble) {
	            $.each(assemble.members, function (member_ind, member) {
	                member.select = $scope.book.selectAll;
	            });
	        });
	    }

	    $scope.agentInvite = function (member, agent) {
	        member.agent = {
	            name: agent.email,
	            agency: null,
	            avatar: null,
	            phoneNumber: null,
	        };
	        agent = null;
	        $scope.newAgentVal = false;
	    }

	    $scope.addcrew = function (ev) {
	        $mdDialog.show({
	            controller: "CrewCreationCtrl",
	            templateUrl: '/partials/productionBook/crew-creation.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,

	        }).then(function (assembles) {
	            $.each(assembles, function (idx, val) {
	                val.crewId = angular.copy(val.id);
	                val.id = $scope.book.assembles.length + 1;
	                val.members = [];
	                $scope.book.assembles.push(val);
	            });
	        }, function () {
	        });
	    };

	    $scope.addNewMember = function (assemble, newmemmber, agent) {
	        $scope.newArtist = false;
	        var member = {
	            name: newmemmber.email,
	            avatar: null,
	            phoneNumber: null,
	            job: null,
	            agent: null,
	            agents: [
                    {
                        name: 'Amy Agent',
                        agency: 'BRYNN CREATIVE',
                        avatar: 'images/data/avatar-11.png',
                        phoneNumber: "(123) 456 7890",
                    },
                    {
                        name: 'Adam Agent',
                        agency: 'BRYNN CREATIVE',
                        avatar: 'images/data/avatar-12.png',
                        phoneNumber: "(123) 456 7890",
                    },
                    {
                        name: 'Brad Agent',
                        agency: 'BRYNN CREATIVE',
                        avatar: 'images/data/avatar-13.png',
                        phoneNumber: "(123) 456 7890",
                    }]
	        };
	        member.id = assemble.members.length + 1;
	        member.rate = '99.99';
	        member.currency = '$';
	        member.time = 'hr';
	        member.dates = [];
	        var startDate = new Date(angular.copy($scope.book.startDate));
	        var endDate = new Date(angular.copy($scope.book.endDate));
	        while (startDate < endDate) {
	            member.dates.push({
	                value: startDate,
	                choice: 0,
	                status: null,
	                selected: false
	            });
	            startDate = new Date(startDate.setDate(startDate.getDate() + 1));
	        }

	        if (!agent) {
	            member.agents = []
	        }
	        assemble.members.push(member);
	        $scope.book.selectAll = false;
	    }

	    $scope.addMember = function (assemble, member) {
	        $scope.newArtist = false;
	        member = angular.copy(member);
	        member.id = assemble.members.length + 1;
	        member.rate = '99.99';
	        member.currency = '$';
	        member.time = 'hr';
	        member.dates = [];
	        var startDate = new Date(angular.copy($scope.book.startDate));
	        var endDate = new Date(angular.copy($scope.book.endDate));
	        while (startDate < endDate) {
	            member.dates.push({
	                value: startDate,
	                choice: 0,
	                status: null,
	                selected: false
	            });
	            startDate = new Date(startDate.setDate(startDate.getDate() + 1));
	        }
	        assemble.members.push(member);
	        $scope.book.selectAll = false;
	    }

	    $scope.removeAssemble = function (index) {
	        $scope.book.assembles.splice(index, 1);
	        $scope.book.selectAll = false;
	    };

	    $scope.confirm = function () {
	        angular.forEach($scope.book.assembles, function (assemble) {
	            angular.forEach(assemble.members, function (member) {
	                angular.forEach(member.dates, function (date) {
	                    if (date.selected)
	                        date.status = 1;
	                    date.selected = false;
	                });
	            });
	        });
	        $scope.onlySelected = false;
	        $scope.onlyAccept = false;
	        $scope.onlyUndo = false;
	    };

	    $scope.acceptdate = function () {
	        angular.forEach($scope.book.assembles, function (assemble) {
	            angular.forEach(assemble.members, function (member) {
	                angular.forEach(member.dates, function (date) {
	                    if (date.selected)
	                        date.status = 2;
	                    date.selected = false;
	                });
	            });
	        });
	        $scope.onlySelected = false;
	        $scope.onlyAccept = false;
	        $scope.onlyUndo = false;
	    };

	    $scope.undo = function () {
	        angular.forEach($scope.book.assembles, function (assemble) {
	            angular.forEach(assemble.members, function (member) {
	                angular.forEach(member.dates, function (date) {
	                    if (date.selected) {
	                        date.status = null;
	                        date.choice = null;
	                    }
	                    date.selected = false;
	                });
	            });
	        });
	        $scope.onlySelected = false;
	        $scope.onlyAccept = false;
	        $scope.onlyUndo = false;
	    };

	    $scope.dateSelect = function (data) {
	        data.selected = !data.selected;
	        if (data.status == 2) {
	            $scope.onlySelected = false;
	            $scope.onlyAccept = false;
	            $scope.onlyUndo = true;
	        } else if (data.status == 1) {
	            $scope.onlySelected = false;
	            $scope.onlyAccept = true;
	            $scope.onlyUndo = false;
	        } else {
	            $scope.onlySelected = true;
	            $scope.onlyAccept = false;
	            $scope.onlyUndo = false;
	        }
	        var count = 0;

	        angular.forEach($scope.book.assembles, function (assemble) {
	            angular.forEach(assemble.members, function (member) {
	                angular.forEach(member.dates, function (date) {
	                    if (date.selected) {
	                        count++;
	                    }
	                });
	            });
	        });
	        if (!count) {
	            $scope.onlySelected = false;
	            $scope.onlyAccept = false;
	            $scope.onlyUndo = false;
	        }
	    }

	    $scope.showNextDate = function (member) {
	        if (!member.begin)
	            member.begin = 0;
	        if (member.dates.length - 5 >= 5)
	            if (member.dates.length - 5 >= member.begin + 5)
	                member.begin = member.begin + 5;
	            else
	                member.begin = member.dates.length - 5;
	    }

	    $scope.showPervDate = function (member) {
	        member.begin = member.begin - 5;
	        if (member.begin < 0)
	            member.begin = 0;
	    }

	    $scope.confirmAllDates = function (member) {
	        if (member) {
	            angular.forEach(member.dates, function (date) {
	                date.status = 1;
	                date.choice = null;
	                date.selected = false;
	            });
	        } else {
	            angular.forEach($scope.book.assembles, function (assemble) {
	                angular.forEach(assemble.members, function (member) {
	                    if (member.select) {
	                        angular.forEach(member.dates, function (date) {
	                            date.status = 1;
	                            date.choice = null;
	                            date.selected = false;
	                        });
	                    }
	                });
	            });
	        }
	    }

	    $scope.resetAllDates = function (member) {
	        if (member) {
	            angular.forEach(member.dates, function (date) {
	                date.status = null;
	                date.choice = null;
	                date.selected = false;
	            });
	        } else {
	            angular.forEach($scope.book.assembles, function (assemble) {
	                angular.forEach(assemble.members, function (member) {
	                    if (member.select) {
	                        angular.forEach(member.dates, function (date) {
	                            date.status = null;
	                            date.choice = null;
	                            date.selected = false;
	                        });
	                    }
	                });
	            });
	        }
	    }

	    $scope.release = function (index, members) {
	        if (members && members.length) {
	            members.splice(index, 1);
	        } else {	            
	            angular.forEach($scope.book.assembles, function (assemble) {
	                var releaseMembers = [];
	                angular.forEach(assemble.members, function (member, index) {
	                    if (member.select) {
	                        releaseMembers.push(member);
	                    }
	                });
	                angular.forEach(releaseMembers, function (member) {
	                    var index = assemble.members.indexOf(member);
	                    assemble.members.splice(index, 1);
	                });
	            });
	        }
	        $scope.book.selectAll = false;
	    }

	});