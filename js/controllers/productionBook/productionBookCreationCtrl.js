angular.module('lenspireApp')
	.controller('ProductionBookCreationCtrl', function ProductionBookCreationCtrl($scope, $mdDialog, $location, CommonService, book) {
	    var originatorEv;
	    $scope.today = new Date();
	    if (!book) {
	        $scope.production = {
	            logo: ['images/data/logo-1.png', 'images/data/logo-2.png', 'images/data/logo-3.png', 'images/data/logo-4.png'],
	            bookName: "",
	            jobNo: "",
	            startDate: null,
	            endDate: null,
	            client: "",
	            agency: "",
	            shootNotes: "",
	            billingInfo: "",
	            usageTerms: "",
	            startDate1: null,
	            endDate1: null,
	            status: 1,
	            crew: []
	        };
	    } else {
	        $scope.production = book;
	    }

	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };

	    $scope.save = function () {
	        var array = [];
	        array.push(angular.copy($scope.production.agency));
	        $scope.production.agency = array;
	        CommonService.setProductinBook($scope.production);
	        var productinBook = CommonService.getProductinBooks();
	        $location.path("/production/"+(productinBook.length)+"/assemble")
	        $mdDialog.hide();
	    };     

	})