﻿angular.module('lenspireApp')
	.controller('SelectCrewCtrl', function SelectCrewCtrl($scope, $mdDialog) {

	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };
	    $scope.addCrews = function (crews) {
	        $mdDialog.hide(crews);
	    };

	    $scope.crewMembers = [{
	        id: 0,
	        avatar: 'images/data/avatar-12.png',
	        artist: 'Art Director',
	        name: 'Nicholas George',
	        phoneNumber: "(123) 456 7890"
	    }, {
	        id: 1,
	        avatar: 'images/data/avatar-14.png',
	        artist: '1st Assistant',
	        name: 'Ellis Hanson',
	        phoneNumber: "(123) 456 7890"
	    }, {
	        id: 2,
	        avatar: 'images/data/avatar-11.png',
	        artist: 'Photographer',
	        name: 'Jack Harrison',
	        phoneNumber: "(123) 456 7890"
	    }];

	    
	    $scope.selectedCrews = [];

	    $scope.selectCerw = function (crew) {
	        crew.selected = !crew.selected;
	        var index = $.inArray(crew, $scope.selectedCrews);
	        if (index == -1) {
	            $scope.selectedCrews.push(crew);
	        } else {
	            $scope.selectedCrews.splice(index, 1);
	        }
	    }

	    $scope.allselectCerw = function () {
	        $scope.selected = !$scope.selected;
	        if ($scope.selected) {
	            angular.forEach($scope.crewMembers, function (val) {
	                val.selected = true;
	            });
	            $scope.selectedCrews = $scope.crewMembers;
	        } else {
	            angular.forEach($scope.crewMembers, function (val) {
	                val.selected = false;
	            });
	            $scope.selectedCrews = [];
	        }
	    }

	});
