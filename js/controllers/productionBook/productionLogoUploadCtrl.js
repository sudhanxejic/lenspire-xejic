﻿angular.module('lenspireApp')
	.controller('ProductionLogoUploadCtrl', function ProductionLogoUploadCtrl($scope, $mdMedia, $mdDialog, CommonService, book) {
	    'use strict';
	    $scope.book = book;

	    $scope.hide = function () {
	        $mdDialog.hide();
	    };

	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };

	    $scope.removeLogo = function (index) {
	        $scope.book.logo[index] = null;
	    }

	});