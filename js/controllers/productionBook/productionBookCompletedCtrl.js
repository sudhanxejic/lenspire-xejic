﻿angular.module('lenspireApp')
	.controller('ProductionBookCompletedCtrl', function ProductionBookCompletedCtrl($scope, $window, $mdSidenav, $mdMedia, $mdDialog, CommonService) {
	    'use strict';

	    $scope.currentNavItem = "completed";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.user = CommonService.getUser();
	    $scope.productionSideBar = "/partials/productionBook/productions-sidebar.html";

	    $scope.crews = [{ id: 1, name: 'Chad Cooper', profile: 'images/data/avatar-10.png' },
            { id: 2, name: 'Heather Greene', profile: 'images/data/avatar-11.png' },
            { id: 3, name: 'Matt Jackson', profile: 'images/data/avatar-12.png' },
            { id: 4, name: 'Lili Regen', profile: 'images/data/avatar-13.png' },
            { id: 5, name: 'Masha Stone', profile: 'images/data/avatar-13.png' }
	    ];

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }
	    $scope.$watch(function () { return $mdMedia('xs') },
           function (screenSize) {
               $scope.screen = screenSize;
               if ($scope.screen) {
                   $scope.sidenavOpen = false;
               }
           }
          );

	    $scope.productinBooks = CommonService.getProductinBooks();

	    $scope.selectedCrews = [];

	    $scope.selectCerw = function (crew) {
	        var index = $.inArray(crew, $scope.selectedCrews);
	        if (index == -1) {
	            $scope.selectedCrews.push(crew);
	        } else {
	            $scope.selectedCrews.splice(index, 1);
	        }
	    }

	    $scope.checkCerw = function (crew) {
	        var index = $.inArray(crew, $scope.selectedCrews);
	        if (index == -1) {
	            return false;
	        } else {
	            return true;
	        }
	    }

	    $scope.selectCerwFilter = function (bookDetail) {
	        var count = 0;
	        $.each($scope.selectedCrews, function (index, selectedCrew) {
	            $.each(bookDetail.crews, function (idx, crew) {
	                if (crew.id == selectedCrew.id)
	                    count++;
	            });
	        });
	        return count == $scope.selectedCrews.length;
	    };

	});

