﻿angular.module('lenspireApp')
	.controller('TravelCtrl', function ScheduleCtrl($scope, $mdSidenav, $window, $mdDialog, $routeParams, CommonService, $mdMedia) {
	    'use strict';
	    $scope.sidebar = "/partials/productionBook/production-sidebar.html";
	    $scope.currentNavItem = "travel";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = "#/production/" + $scope.book.id + "/" + navItem;
	    }
	    $scope.book = CommonService.getProductinBook($routeParams.productionID);
	    $scope.numberOfCrews = 2;
	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen) {
                      $scope.sidenavOpen = false;
                  } 
              }
             );

	    $scope.$watch(function () { return $mdMedia('gt-md') },
       function (tiny) {
           if (tiny) {
               $scope.numberOfCrews = 5;
           }
           else {
               $scope.numberOfCrews = 2;
           }
       }
      );

	    $scope.itinerarys = [{
	        name: "Early Bird Crew",
	        travelers: [{
	            id: 1,
	            avatar: 'images/data/avatar-1.png',
	            artist: 'Art Director',
	            name: 'Nicholas George',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 2,
	            avatar: 'images/data/avatar-2.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 3,
	            avatar: 'images/data/avatar-3.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 4,
	            avatar: 'images/data/avatar-4.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson'
	        }, {
	            id: 5,
	            avatar: 'images/data/avatar-5.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 6,
	            avatar: 'images/data/avatar-3.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 7,
	            avatar: 'images/data/avatar-4.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 8,
	            avatar: 'images/data/avatar-5.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }],
	        datas: [{
	            date: "AUG 27, 2016 ",
	            values: [{
	                type: 'flight',
	                flightNumber: "ABCD1234",
	                departTime: "7:00 AM",
	                from: "LAX",
	                arrivalTime: "8:15 AM",
	                to: "SFO",
	                notes: "Terminal 3",
	                attachments: null
	            }, {
	                type: 'car',
	                pickUpTime: "9:00 AM",
	                pickUpLocation: "99 GROVE ST SAN FRANCISCO, CA 94102",
	                dropOffLocation: "1333 OLD BAYSHORE HWY BURLINGAME, CA 94010",
	                notes: "Catching a ride with Chad.",
	                attachments: null
	            }]
	        }]
	    }, {
	        name: "Model Crew",
	        travelers: [{
	            id: 1,
	            avatar: 'images/data/avatar-1.png',
	            artist: 'Art Director',
	            name: 'Nicholas George',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 2,
	            avatar: 'images/data/avatar-2.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 3,
	            avatar: 'images/data/avatar-3.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 4,
	            avatar: 'images/data/avatar-4.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson'
	        }, {
	            id: 5,
	            avatar: 'images/data/avatar-5.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }],
	        datas: [{
	            date: "AUG 28, 2016 ",
	            values: [{
	                type: 'flight',
	                flightNumber: "ABCD1234",
	                departTime: "7:00 AM",
	                from: "LAX",
	                arrivalTime: "8:15 AM",
	                to: "SFO",
	                notes: "Terminal 3",
	                attachments: null
	            }, {
	                type: 'car',
	                pickUpTime: "9:00 AM",
	                pickUpLocation: "99 GROVE ST SAN FRANCISCO, CA 94102",
	                dropOffLocation: "1333 OLD BAYSHORE HWY BURLINGAME, CA 94010",
	                notes: "Catching a ride with Chad.",
	                attachments: null
	            }]
	        }]
	    }, {
	        name: "Photographer Crew",
	        travelers: [{
	            id: 1,
	            avatar: 'images/data/avatar-1.png',
	            artist: 'Art Director',
	            name: 'Nicholas George',
	            phoneNumber: '(123) 456 7890'
	        }, {
	            id: 2,
	            avatar: 'images/data/avatar-2.png',
	            artist: '1st Assistant',
	            name: 'Ellis Hanson',
	            phoneNumber: '(123) 456 7890'
	        }],
	        datas: [{
	            date: "AUG 28, 2016 ",
	            values: [{
	                type: 'flight',
	                flightNumber: "ABCD1234",
	                departTime: "7:00 AM",
	                from: "LAX",
	                arrivalTime: "8:15 AM",
	                to: "SFO",
	                notes: "Terminal 3",
	                attachments: null
	            }, {
	                type: 'car',
	                pickUpTime: "9:00 AM",
	                pickUpLocation: "99 GROVE ST SAN FRANCISCO, CA 94102",
	                dropOffLocation: "1333 OLD BAYSHORE HWY BURLINGAME, CA 94010",
	                notes: "Catching a ride with Chad.",
	                attachments: null
	            }]
	        }]
	    }];

	    $scope.addItinerary = function (ev) {
	        $mdDialog.show({
	            controller: "ItineraryCreationCtrl",
	            templateUrl: '/partials/productionBook/itinerary-creation.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: false,
	            fullscreen: true,
	            locals: {
	                itineraryData: {
	                    book: $scope.book
	                }
	            }
	        }).then(function (itinerary) {
	            if (itinerary)
	                $scope.itinerarys.push(itinerary);
	        });
	    }

	    $scope.editItinerary = function (itinerary, ev) {
	        $mdDialog.show({
	            controller: "ItineraryCreationCtrl",
	            templateUrl: '/partials/productionBook/itinerary-creation.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: false,
	            fullscreen: true,
	            locals: {
	                itineraryData: {
	                    book: $scope.book,
	                    itinerary: itinerary
	                }
	            }
	        }).then(function (itinerary) {

	        });
	    }

	    $scope.addTraveler = function (ev, itinerary) {
	        $mdDialog.show({
	            controller: "AddCrewCtrl",
	            templateUrl: '/partials/productionBook/add-crew.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	        }).then(function (crews) {
	            angular.forEach(crews, function (val) {
	                itinerary.travelers.push(val);
	            });
	        });
	    };

	    $scope.deleteItinerary = function ($index) {
	        $scope.itinerarys.splice($index, 1);
	    }
	    $scope.removeCrews = function ($index, value) {
	        value.splice($index, 1);
	    }
	});