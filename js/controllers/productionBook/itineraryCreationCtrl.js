﻿angular.module('lenspireApp')
	.controller('ItineraryCreationCtrl', function CrewCreationCtrl($scope, $filter, $mdDialog, itineraryData, CommonService, $mdMedia) {

	    $scope.book = itineraryData.book;
	    $scope.car = {};
	    $scope.hotel = {};
	    $scope.flight = {};
	    $scope.numberOfCrews = 3;
	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };

	    $scope.$watch(function () { return $mdMedia('gt-sm') },
       function (tiny) {
           if (tiny) {
               $scope.numberOfCrews = 5;
           }
           else {
               $scope.numberOfCrews = 3;
           }
       }
      );

	    $scope.saveitinerary = function () {
	        $mdDialog.hide($scope.itinerary);
	    };

	    if (!itineraryData.itinerary) {
	        $scope.itinerary = {
	            name: "",
	            travelers: [],
	            datas: []
	        }
	    } else {
	        $scope.itinerary = itineraryData.itinerary;
	    }

	    var startDate = angular.copy($scope.book.startDate);
	    while (startDate < $scope.book.endDate) {
	        $scope.itinerary.datas.push({
	            date: $filter('date')(startDate, 'MMM dd, yyyy'),
	            values: []
	        });
	        startDate = new Date(startDate);
	        startDate = new Date(startDate.setDate(startDate.getDate() + 1));
	    }

	    $scope.addCar = function () {
	        for (var i = 0; i < $scope.itinerary.datas.length; i++) {
	            if ($scope.itinerary.datas[i].date == $filter('date')($scope.car.pickUpDate, 'MMM dd, yyyy')) {
	                $scope.car.type = "car";
	                $scope.itinerary.datas[i].values.unshift($scope.car);
	                $scope.car = {};
	                break;
	            }
	        }
	    }

	    $scope.addFlight = function () {
	        for (var i = 0; i < $scope.itinerary.datas.length; i++) {
	            if ($scope.itinerary.datas[i].date == $filter('date')($scope.flight.departDate, 'MMM dd, yyyy')) {
	                $scope.flight.type = "flight";
	                $scope.itinerary.datas[i].values.unshift($scope.flight);
	                $scope.flight = {};
	                break;
	            }
	        }
	    }

	    $scope.addHotel = function () {
	        for (var i = 0; i < $scope.itinerary.datas.length; i++) {
	            if ($scope.itinerary.datas[i].date == $filter('date')($scope.hotel.checkInDate, 'MMM dd, yyyy')) {
	                $scope.hotel.type = "hotel";
	                $scope.itinerary.datas[i].values.unshift($scope.hotel);
	                $scope.hotel = {};
	                break;
	            }
	        }
	    }


	    $scope.addTraveler = function (ev) {
	        $mdDialog.show({
	            skipHide: true,
	            controller: "AddCrewCtrl",
	            templateUrl: '/partials/productionBook/add-crew.html',
	            parent: angular.element(document.querySelector('#itinerary-creation')),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	        }).then(function (crews) {
	            angular.forEach(crews, function (val) {
	                $scope.itinerary.travelers.push(val);
	            });
	        });
	    }

	    $scope.removeCrews = function ($index, value) {
	        value.splice($index, 1);
	    }

	    $scope.removeItinerary = function ($index, value) {
	        value.splice($index, 1);
	    }
	});
