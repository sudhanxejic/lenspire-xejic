﻿angular.module('lenspireApp')
	.controller('AttachmentCtrl', function AttachmentCtrl($scope, $mdSidenav, $window, $mdDialog, $routeParams, CommonService, $mdMedia) {
	    'use strict';
	    $scope.sidebar = "/partials/productionBook/production-sidebar.html";
	    $scope.currentNavItem = "attachments";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = "#/production/" + $scope.book.id + "/" + navItem;
	    }
	    $scope.book = CommonService.getProductinBook($routeParams.productionID);
	    $scope.beginer = 0;

	    $scope.sort = 'NEWEST FIRST';

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen == true) {
                      $scope.sidenavOpen = false;
                  }
              }
             );

	    $scope.sharedFiles = [{
	        crew: [],
	        filename: 'invoice1.pdf',
	        size: '1',
	        label: 'billing',
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice2.pdf',
	        size: '1',
	        label: 'production',
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice3.pdf',
	        size: '1',
	        label: 'story',
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice4.pdf',
	        size: '1',
	        label: 'bts',
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice5.pdf',
	        size: '1',
	        label: null,
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice6.pdf',
	        size: '1',
	        label: null,
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice7.pdf',
	        size: '1',
	        label: null,
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice8.pdf',
	        size: '1',
	        label: null,
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice9.pdf',
	        size: '1',
	        label: null,
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }, {
	        crew: [],
	        filename: 'invoice10.pdf',
	        size: '1',
	        label: null,
	        posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' }
	    }];

	    $scope.sharedwink = [{
	        date: 'JULY 1, 2016',
	        name: 'Andrea Lewis',
	        profession: 'Spring Fashion',
	        crews: [],
	        winks: ['images/data/photo-9.png', 'images/data/photo-3.png', 'images/data/photo-6.png']
	    }, {
	        date: 'JULY 1, 2016',
	        name: 'Andrea Lewis',
	        profession: 'Spring Fashion',
	        winks: ['images/data/photo-2.png', 'images/data/photo-11.png'],
	        crews: []
	    }, {
	        date: 'JULY 1, 2016',
	        name: 'Andrea Lewis',
	        profession: 'Shooting Locations',
	        crews: [],
	        winks: ['images/data/photo-8.png']

	    }, {
	        date: 'JULY 1, 2016',
	        name: 'Andrea Lewis',
	        profession: 'Concepts',
	        crews: [],
	        winks: ['images/data/photo-2.png', 'images/data/photo-11.png'],

	    }, ];

	    $scope.setLabel = function (value, option) {
	        option = value;
	    }

	    $scope.addcrewdialog = function (ev, sharedFiles) {
	        $mdDialog.show({
	            controller: "AddCrewCtrl",
	            templateUrl: '/partials/productionBook/add-crew.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	        }).then(function (crews) {
	            angular.forEach(crews, function (val) {
	                sharedFiles.crew.push(val);
	            });
	        });
	    };

	    $scope.sharedwinkdialog = function (ev, sharedwink) {
	        $mdDialog.show({
	            controller: "AddCrewCtrl",
	            templateUrl: '/partials/productionBook/add-crew.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	        }).then(function (crews) {
	            angular.forEach(crews, function (val) {
	                sharedwink.crews.push(val);
	            });
	        });
	    };

	    $scope.sharedwinkboard = function (ev) {
	        $mdDialog.show({
	            controller: "SharedWinkboardCtrl",
	            templateUrl: '/partials/productionBook/shared-wink-board.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	        });
	    }

	    $scope.upload = function (upload) {
	        $scope.sharedFiles.push({
	            crew: [],
	            filename: upload.name,
	            size: (upload.size / 1048576),
	            label: null,
	            posted: { avatar: 'images/data/avatar-12.png', date: 'JULY 1, 2016', name: 'Andrea Lewis' },
	            sharedwith: ''
	        });
	    }

	    $scope.removeCrew = function ($index, value) {
	        value.splice($index, 1);
	    }

	    $scope.removeSharedFiles = function (ev, index) {
	        var confirm = $mdDialog.confirm()
              .title('Are you sure you want to remove this attachments?')
              .ariaLabel('Remove Shared Files')
              .targetEvent(ev)
              .hasBackdrop(false)
              .clickOutsideToClose(true)
              .ok('YES')
              .cancel('NO');

	        $mdDialog.show(confirm).then(function () {
	            $scope.sharedFiles.splice(index, 1);
	        });
	    }
	});