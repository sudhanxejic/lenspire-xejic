﻿angular.module('lenspireApp')
	.controller('SharedWinkboardCtrl', function SharedWinkboardCtrl($scope, $mdDialog, CommonService) {

	    $scope.hide = function () {
	        $mdDialog.hide();
	    };
	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };
	    $scope.save = function () {
	        $mdDialog.hide();
	    }
	    $scope.sharedwink = [{
	        caption: 'Nature',
	        winks: ['images/data/photo-9.png', 'images/data/photo-3.png', 'images/data/photo-6.png']
	    }, {
	        caption: 'Shooting Locations',
	        winks: ['images/data/photo-2.png', 'images/data/photo-11.png']
	    }, {
	        caption: 'Concepts',
	        winks: ['images/data/photo-8.png']

	    }, {
	        caption: 'Nature',
	        winks: ['images/data/photo-2.png', 'images/data/photo-11.png']
	    }, ];
	});