﻿angular.module('lenspireApp')
	.controller('CrewCreationCtrl', function CrewCreationCtrl($scope, $mdDialog, $filter, CommonService) {
	    $scope.hide = function () {
	        $mdDialog.hide();
	    };
	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };

	    $scope.crewSelected = function (crew) {
	        crew.selected = !crew.selected;
	    }

	    $scope.getMobileOperatingSystem = function () {
	        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
	        if (/windows phone/i.test(userAgent)) {
	            return "Windows Phone";
	        }
	        if (/android/i.test(userAgent)) {
	            return "Android";
	        }
	        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
	            return "iOS";
	        }
	        return "unknown";
	    }


	    $scope.assemble = function () {
	        $scope.addedcrews
	        $.each($scope.addedcrews, function (index, val) {
	            val.members = [{
	                name: 'Chad Cooper',
	                avatar: 'images/data/avatar-6.png',
	                agency: 'BRYNN CREATIVE',
	                agent: 'Amy Agent',
	                agentavatar: 'images/data/avatar-11.png',
	            }, {
	                name: 'Chad Cooper',
	                avatar: 'images/data/avatar-7.png',
	                agency: 'BRYNN CREATIVE',
	                agent: 'Amy Agent',
	                agentavatar: 'images/data/avatar-11.png',
	            }, {
	                name: 'Chad Cooper',
	                avatar: 'images/data/avatar-8.png',
	                agency: 'BRYNN CREATIVE',
	                agent: 'Amy Agent',
	                agentavatar: 'images/data/avatar-11.png',
	            }];
	        });
	        $mdDialog.hide($scope.addedcrews);
	    };

	    $scope.showtemplate = false;
	    $scope.crews = CommonService.getCrews();
	    $scope.addedcrews = [];
	    $scope.leftOption = [];
	    $scope.rightOption = [];

	    $scope.addCrew = function () {
	        if ($.trim($scope.crew) !== "") {
	            var newCrew = {
	                name: $scope.crew
	            };
	            $scope.crews.push(newCrew);
	            CommonService.setCrew(newCrew);
	            $scope.crew = null;
	        }
	    }

	    $scope.add = function () {
	        $.each($scope.leftOption, function (index, val) {
	            $scope.addedcrews.push(val);
	            $scope.crews.splice($scope.crews.indexOf(val), 1);
	        })
	    }

	    $scope.addList = function () {
	        var leftOption = $filter('filter')($scope.crews, { selected: true });
	        $.each(leftOption, function (index, val) {
	            val.selected = false;
	            $scope.addedcrews.push(val);
	            $scope.crews.splice($scope.crews.indexOf(val), 1);
	        })
	    }

	    $scope.remove = function () {
	        $.each($scope.rightOption, function (index, val) {
	            $scope.crews.push(val);
	            $scope.addedcrews.splice($scope.addedcrews.indexOf(val), 1);
	        })
	    }

	    $scope.removeList = function () {
	        var rightOption = $filter('filter')($scope.addedcrews, { selected: true });
	        $.each(rightOption, function (index, val) {
	            val.selected = false;
	            $scope.crews.push(val);
	            $scope.addedcrews.splice($scope.addedcrews.indexOf(val), 1);
	        })
	    }

	});
