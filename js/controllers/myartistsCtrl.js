angular.module('lenspireApp')
	.controller('MyArtistsCtrl', function MyArtistsCtrl($scope, $mdMedia, $mdSidenav, $window) {
	    'use strict';
	    $scope.currentNavItem = "myartists";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	   
	    $scope.myartistsTemplate = "/partials/layout/profile.html";
	    $scope.sortBy = 'alphabetically';

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen == true) {
                      $scope.sidenavOpen = false;
                  }
              }
             );

	    $scope.setSortBy = function (_val) {
	        $scope.sortBy = _val;
	    }

	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.artistsList = [{
	        name: "Thomas Burke",
	        avatar: "images/data/artists-4.png",
	        job: "ART DIRECTOR"
	    }, {
	        name: "Dalia Kenwood",
	        avatar: "images/data/artists-2.png",
	        job: "PHOTOGRAPHER"
	    }, {
	        name: "Heather Myers",
	        avatar: "images/data/artists-3.png",
	        job: "SET DESIGNER"
	    }, {
	        name: "Bruce Salazar",
	        avatar: "images/data/artists-4.png",
	        job: "MODEL"
	    }];

	});
