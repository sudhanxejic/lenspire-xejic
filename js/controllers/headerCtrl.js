angular.module('lenspireApp')
	.controller('HeaderCtrl', function HeaderCtrl($scope, $location, $window, $timeout, CommonService) {
	    'use strict';
	    $scope.user = CommonService.getUser();
	    $scope.getPageName = function () {
	        var path = $location.path();
	        if (path == '/home' || path == "/" || path == "/story" || path == "/behindscene" || path == "/credits")
	            return "Home";
	        else if (path.indexOf('/production')!= -1)
	            return "Production";
	        else if (path == '/calendar')
	            return "Calendar";
	        else if (path.indexOf('/admin') != -1)
	            return "Admin";
	        else if (path == '/portfolios')
	            return "portfolios";
	        return "";
	    };
        
	    $scope.messages = [{
	        profileUrl: "images/data/avatar-1.png",
	        fromText: "Marilyn Salazar",
	        to: "Thanks for reaching ou. i recently just got ...",
	        date: "5m",
	        read: false
	    }, {
	        profileUrl: "images/data/avatar-2.png",
	        fromText: "Walter Hanson",
	        to: "Hey! Can you send me the photos from the...",
	        date: "4h",
	        read: false
	    }, {
	        profileUrl: "images/data/avatar-3.png",
	        fromText: "Catherine Weber +1 ",
	        to: "I will book the hotel rooms for next week's...",
	        date: "1d",
	        read: true
	    }, {
	        profileUrl: "images/data/avatar-4.png",
	        fromText: "Benjamin Wood  ",
	        to: "Nice Wink Boards!",
	        date: "1w",
	        read: true
	    }];

	    $scope.notifications = [{
	        status: 1,
	        avatar: "images/data/avatar-1.png",
	        notificationText: "<b>Marilyn Salazar</b> has credited you as 'Photographer' in <b>Summer Collection 2016</b>. Do you accept?",
	        date: "5m",
	        read: false
	    }, {

	        status: 2,
	        avatar: "images/data/avatar-2.png",
	        notificationText: "<b>Walter Hanson</b> started following you.",
	        date: "4h",
	        read: false
	    }, {
	        status: 3,
	        avatar: "images/data/notify-ava-1.png",
	        notificationText: "<b>Lenspire Team</b> <br> Congratulations! You have been upgrade to a premium user. You can now enjoy all the <b> premium features</b> Lenspire has to offer!",
	        date: "1d",
	        read: true
	    }];

	    $timeout(function () {
	        $('.page-link').on('click', function (e) {
	            $('#header-navbar.in').collapse('hide');
	        });
	    }, 0)
	});
