angular.module('lenspireApp')
	.controller('TalentCtrl', function TalentCtrl($scope, $mdMedia, $mdSidenav, $window) {
	    'use strict';
	    $scope.currentNavItem = "talent";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.myartistsTemplate = "/partials/layout/profile-agent.html";
	    $scope.sortBy = 'name';

	    $scope.setSortBy = function (_val) {
	        $scope.sortBy = _val;
	    }

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen == true) {
                      $scope.sidenavOpen = false;
                  }
              }
             );

	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.artistsList = [{
	        name: "Thomas Burke",
	        avatar: "images/data/artists-1.png",
	        role: "ART DIRECTOR"
	    }, {
	        name: "Julia Cooper",
	        avatar: "images/data/artists-2.png",
	        role: "ART DIRECTOR"
	    }, {
	        name: "David Gang",
	        avatar: "images/data/artists-3.png",
	        role: "COPYWRITER"
	    }, {
	        name: "Dalia Kenwood",
	        avatar: "images/data/artists-4.png",
	        role: "PHOTOGRAPHER"
	    }, {
	        name: "Kevin Lemmings",
	        avatar: "images/data/artists-5.png",
	        role: "WARDROBE"
	    }, {
	        name: "Heather Myers",
	        avatar: "images/data/artists-6.png",
	        role: "SET DESIGNER"
	    }, {
	        name: "Bruce Salazar",
	        avatar: "images/data/artists-7.png",
	        role: "MODEL"
	    }, {
	        name: "Joyce Tan",
	        avatar: "images/data/artists-8.png",
	        role: "MODEL"
	    }, {
	        name: "Peter Wang",
	        avatar: "images/data/artists-9.png",
	        role: "PHOTOGRAPHER"
	    }];

	});
