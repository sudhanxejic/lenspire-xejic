angular.module('lenspireApp')
	.controller('TalentSelectCtrl', function TalentSelectCtrl($scope, $mdDialog) {

	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };
	    $scope.selected = {
	        id: 1,
	        avatar: 'images/data/avatar-1.png',
	        name: 'Thomas Burke',
	        role: "Art Director",
	    }

	    $scope.addToPortfolio = function () {
	        var data = {
	            image: "images/data/photo-14.png",
	            publishers: [{
	                publisherName: "Cosmopolitan",
	                publisherAvatar: "images/data/avatar-1.png"
	            }],
	            winkName: "Fall Collection",
	            active: false,
	            public: true
	        }

	        $mdDialog.hide(data);
	    };

	    $scope.pressed = false;

	    $scope.talents = [{
	        id: 1,
	        avatar: 'images/data/avatar-1.png',
	        name: 'Thomas Burke',
	        role: "Art Director",
	    }, {
	        id: 2,
	        avatar: 'images/data/avatar-2.png',
	        name: 'Julia Cooper',
	        role: "Art Director",
	    }, {
	        id: 3,
	        avatar: 'images/data/avatar-3.png',
	        name: 'David Gang',
	        role: "Head Of Marketing",
	    }, {
	        id: 4,
	        avatar: 'images/data/avatar-4.png',
	        name: 'Dalia Kenwood',
	        role: "Photographer",
	    }, {
	        id: 5,
	        avatar: 'images/data/avatar-5.png',
	        name: 'Heather Myers',
	        role: "Set Designer",
	    }, {
	        id: 6,
	        avatar: 'images/data/avatar-6.png',
	        name: 'Bruce Salazar',
	        role: "Model",
	    }, {
	        id: 7,
	        avatar: 'images/data/avatar-7.png',
	        name: 'Joyce Tan',
	        role: "Model",
	    }, {
	        id: 8,
	        avatar: 'images/data/avatar-8.png',
	        name: 'Thomas Burke',
	        role: "Art Director",
	    }, {
	        id: 9,
	        avatar: 'images/data/avatar-9.png',
	        name: 'Julia Cooper',
	        role: "Art Director",
	    }, {
	        id: 10,
	        avatar: 'images/data/avatar-10.png',
	        name: 'David Gang',
	        role: "Head Of Marketing",
	    }, {
	        id: 11,
	        avatar: 'images/data/avatar-11.png',
	        name: 'Dalia Kenwood',
	        role: "Photographer",
	    }, {
	        id: 12,
	        avatar: 'images/data/avatar-12.png',
	        name: 'Heather Myers',
	        role: "Set Designer",
	    }, {
	        id: 13,
	        avatar: 'images/data/avatar-13.png',
	        name: 'Bruce Salazar',
	        role: "Model",
	    }, {
	        id: 14,
	        avatar: 'images/data/avatar-14.png',
	        name: 'Joyce Tan',
	        role: "Model",
	    }];

	    $scope.match = function (input) {
	        $scope.selected = input;
	    }
	})