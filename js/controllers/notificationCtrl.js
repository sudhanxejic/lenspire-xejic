angular.module('lenspireApp')
	.controller('NotificationCtrl', function NotificationCtrl($scope) {
	    'use strict';
	    $scope.notifications = [{
	        status: 1,
	        avatar: "images/data/avatar-1.png",
	        notificationText: "<b>Marilyn Salazar</b> has credited you as 'Photographer' in <b>Summer Collection 2016</b>. Do you accept?",
	        date: "5 minutes ago",
	        read: false
	    }, {

	        status: 2,
	        avatar: "images/data/avatar-2.png",
	        notificationText: "<b>Walter Hanson</b> started following you.",
	        date: "4 hours ago",
	        read: false
	    }, {
	        status: 3,
	        avatar: "images/data/notify-ava-1.png",
	        notificationText: "<b>Lenspire Team</b>: Congratulations! You have been upgrade to a premium user. You can now enjoy all the <b> premium features</b> Lenspire has to offer!",
	        date: "1 day ago",
	        read: true
	    }, {
	        status: 3,
	        avatar: "images/data/avatar-3.png",
	        notificationText: "<b>Devin Scott</b> has winked your <b>Digital Media</b>.",
	        date: "4 days ago",
	        read: true
	    }, {
	        status: 3,
	        avatar: "images/data/avatar-4.png",
	        notificationText: "<b>Charlene Lee</b> has winked your <b>Sports Photoshoot</b>",
	        date: "4 week ago",
	        read: true
	    }, {
	        status: 2,
	        avatar: "images/data/avatar-5.png",
	        notificationText: "<b>Simon Dominic</b> started following you.",
	        date: "1 week ago",
	        read: true
	    }, {
	        status: 3,
	        avatar: "images/data/avatar-6.png",
	        notificationText: "<b>Karen Sher</b> has added a new attachment in Production Book <b>GUCCI SUMMER PHOTOSHOOT</b>.",
	        date: "1 week ago",
	        read: true
	    }, {
	        status: 3,
	        avatar: "images/data/avatar-7.png",
	        notificationText: "<b>Sallie Madriz</b> has added a new attachment in Production Book <b>GUCCI SUMMER PHOTOSHOOT</b>.",
	        date: "1 week ago",
	        read: true
	    }, {
	        status: 3,
	        avatar: "images/data/avatar-7.png",
	        notificationText: "<b>Sallie Madriz</b> has added a new attachment in Production Book <b>GUCCI SUMMER PHOTOSHOOT</b>.",
	        date: "1 week ago",
	        read: true
	    }];

	});
