﻿angular.module('lenspireApp')
	.controller('ProductReleasesCtrl', function ProductReleasesCtrl($scope, $window) {
	    'use strict';
	    $scope.currentNavItem = "product";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.short = function (detail) {
	        $scope.myOrder = detail;
	    }
	});