﻿angular.module('lenspireApp')
	.controller('LoginHistoryCtrl', function LoginHistoryCtrl($scope, $window) {
	    'use strict';

	    $scope.currentNavItem = "login";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.short = function (detail) {
	        $scope.myOrder = detail;
	    }

	    $scope.historyDetail = [{
	        name: 'John Doe',
	        Email: 'jdoe@email.com',
	        Role: 'Artist',
	        Description: 'Copywriter',
	        Login: '1',
	        LastRequested: 'Web',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }, {
	        name: 'John Doe',
	        Email: 'jdoe@email.com',
	        Role: 'Artist',
	        Description: 'Copywriter',
	        Login: '2',
	        LastRequested: 'Web',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }, {
	        name: 'John Doe',
	        Email: 'jdoe@email.com',
	        Role: 'Artist',
	        Description: 'Copywriter',
	        Login: '3',
	        LastRequested: 'Web',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }, {
	        name: 'John Doe',
	        Email: 'jdoe@email.com',
	        Role: 'Artist',
	        Description: 'Copywriter',
	        Login: '4',
	        LastRequested: 'Web',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }, {
	        name: 'John Doe',
	        Email: 'jdoe@email.com',
	        Role: 'Artist',
	        Description: 'Copywriter',
	        Login: '5',
	        LastRequested: 'Web',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }]


	});
