﻿angular.module('lenspireApp')
	.controller('AddCompanyCtrl', function AddCompanyCtrl($scope, $mdDialog) {
	    $scope.company = null;

	    $scope.addCompany = function () {
	        if ($scope.company) {
	            var company = {
	                company: $scope.company.companyName,
	                companyEmail: $scope.company.email,
	                companyPhone: $scope.company.phoneNumber,
	                contactPerson: $scope.company.contactFirstName?$scope.company.contactFirstName:"" + " " + $scope.company.contactLastName?$scope.company.contactLastName:"",
	                website: $scope.company.website
	            }
	            $mdDialog.hide(company);
	        }
	    };

	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };

	})