﻿angular.module('lenspireApp')
	.controller('AuditLogCtrl', function AuditLogCtrl($scope, $window) {
	    'use strict';

	    $scope.currentNavItem = "audit";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.short = function (detail) {
	        $scope.myOrder = detail;
	    }

	    $scope.auditDetail = [{
	        name: 'John Doe',
	        Module: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }, {
	        name: 'John Doe',
	        Module: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }, {
	        name: 'John Doe',
	        Module: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }, {
	        name: 'John Doe',
	        Module: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
	        Date: '05-23-2016 0:6:12:00 CT'
	    }];

	});
