﻿angular.module('lenspireApp')
	.controller('CouponCtrl', function CouponCtrl($scope, $mdDialog, $mdMedia, $window) {
	    'use strict';

	    $scope.currentNavItem = "coupon";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.short = function (detail) {
	        $scope.myOrder = detail;
	    }

	    $scope.couponDetail = [{
	        date: '05-23-2016 0:6:12:00 CT',
	        code: 'ABCDE12345',
	        description: 'Lorem ipsum dolor sit amet',
	        expiry: '05-23-2016 0:6:12:00 CT',
	        status: 'ACTIVE'
	    }, {
	        date: '05-23-2016 0:6:12:00 CT',
	        code: 'ABCDE12345',
	        description: 'Lorem ipsum dolor sit amet',
	        expiry: '05-23-2016 0:6:12:00 CT',
	        status: 'ACTIVE'
	    }, {
	        date: '05-23-2016 0:6:12:00 CT',
	        code: 'ABCDE12345',
	        description: 'Lorem ipsum dolor sit amet',
	        expiry: '05-23-2016 0:6:12:00 CT',
	        status: 'DEACTIVE'
	    }, {
	        date: '05-23-2016 0:6:12:00 CT',
	        code: 'ABCDE12345',
	        description: 'Lorem ipsum dolor sit amet',
	        expiry: '05-23-2016 0:6:12:00 CT',
	        status: 'ACTIVE'
	    }, {
	        date: '05-23-2016 0:6:12:00 CT',
	        code: 'ABCDE12345',
	        description: 'Lorem ipsum dolor sit amet',
	        expiry: '05-23-2016 0:6:12:00 CT',
	        status: 'ACTIVE'
	    }, {
	        date: '05-23-2016 0:6:12:00 CT',
	        code: 'ABCDE12345',
	        description: 'Lorem ipsum dolor sit amet',
	        expiry: '05-23-2016 0:6:12:00 CT',
	        status: 'ACTIVE'
	    }, ]

	    $scope.stat = '  ';

	    $scope.couponCreate = function (ev) {
	        $mdDialog.show({
	            controller: "GenerateCouponCtrl",
	            templateUrl: '/partials/admin/generateCoupon.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true
	        })
            .then(function () {

            }, function () { });
	    };
	});
