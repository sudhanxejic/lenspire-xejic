﻿angular.module('lenspireApp')
	.controller('SentLogCtrl', function SentLogCtrl($scope, $window) {
	    'use strict';

	    $scope.currentNavItem = "sent";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.short = function (detail) {
	        $scope.myOrder = detail;
	    }

	    $scope.sentDetail = [{
	        SentDate: '05-23-2016 0:6:12:00 CT',
	        EmailFrom: 'jdoe@email.com',
	        EmailTo: 'jsmith@email.com',
	        Subject: 'Invitation for production book',
	        Type: 'Email',
	        Message: 'Web Assemble Crew',
	        Module: 'Modify Artist Rate',
	        Status: 'Success'
	    }, {

	        SentDate: '05-23-2016 0:6:12:00 CT',
	        EmailFrom: 'jdoe@email.com',
	        EmailTo: 'jsmith@email.com',
	        Subject: 'Invitation for production book',
	        Type: 'Email',
	        Message: 'Web Assemble Crew',
	        Module: 'Publish Production Book',
	        Status: 'Success'
	    }, {

	        SentDate: '05-23-2016 0:6:12:00 CT',
	        EmailFrom: 'jdoe@email.com',
	        EmailTo: 'jsmith@email.com',
	        Subject: 'Invitation for production book',
	        Type: 'Email',
	        Message: 'Web Assemble Crew',
	        Module: 'Add Artist to Crew',
	        Status: 'Success'
	    }, {

	        SentDate: '05-23-2016 0:6:12:00 CT',
	        EmailFrom: 'jdoe@email.com',
	        EmailTo: 'jsmith@email.com',
	        Subject: 'Invitation for production book',
	        Type: 'Email',
	        Message: 'Web Assemble Crew',
	        Module: 'Confirm Artist',
	        Status: 'Success'
	    }, {

	        SentDate: '05-23-2016 0:6:12:00 CT',
	        EmailFrom: 'jdoe@email.com',
	        EmailTo: 'jsmith@email.com',
	        Subject: 'Invitation for production book',
	        Type: 'Email',
	        Message: 'Web Assemble Crew',
	        Module: 'AcceptArtist',
	        Status: 'Success'
	    }]



	});
