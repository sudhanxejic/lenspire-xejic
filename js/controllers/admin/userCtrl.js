﻿angular.module('lenspireApp')
	.controller('UserCtrl', function UserCtrl($scope, $mdDialog, $mdMedia, $window) {
	    'use strict';

	    $scope.currentNavItem = "users";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }

	    $scope.category = 'PENDING';
	    $scope.categories =  [
            { id: 'PENDING', name: 'Request Pending' },
            { id: 'INACTIVE', name: 'Inactive' },
            { id: 'APPROVED', name: 'Approved' },
            { id: 'DECLINED', name: 'Declined' },
            { id: 'REGISTERED', name: 'Registered' },
            { id: null, name: 'All' }
	    ];
	    $scope.users = [];
	    $scope.usersList = [{
	        name: "John Doe",
	        email: "jdoe@email.com",
	        phone: "+123 456 7890",
	        role: "Producer",
	        status: "PENDING",
	        requestedDates: "05-23-2016 0:6:12:00 CT",
	        approvedBy: "",
	        approvedDate: "",
	        declineBy: "",
	        declineDate: ""
	    }, {
	        name: "John Doe",
	        email: "jdoe@email.com",
	        phone: "+123 456 7890",
	        role: "Producer",
	        status: "APPROVED",
	        requestedDates: "05-23-2016 0:6:12:00 CT",
	        approvedBy: "Jane Smith",
	        approvedDate: "05-23-2016 0:6:12:00 CT",
	        declineBy: "",
	        declineDate: ""
	    }, {
	        name: "John Doe",
	        email: "jdoe@email.com",
	        phone: "+123 456 7890",
	        role: "Producer",
	        status: "APPROVED",
	        requestedDates: "05-23-2016 0:6:12:00 CT",
	        approvedBy: "Jane Smith",
	        approvedDate: "05-23-2016 0:6:12:00 CT",
	        declineBy: "",
	        declineDate: ""
	    }, {
	        name: "John Doe",
	        email: "jdoe@email.com",
	        phone: "+123 456 7890",
	        role: "Producer",
	        status: "DECLINED",
	        requestedDates: "05-23-2016 0:6:12:00 CT",
	        approvedBy: "",
	        approvedDate: "",
	        declineBy: "Jane Smith",
	        declineDate: "05-23-2016 0:6:12:00 CT"
	    }, {
	        name: "John Doe",
	        email: "jdoe@email.com",
	        phone: "+123 456 7890",
	        role: "Producer",
	        status: "DECLINED",
	        requestedDates: "05-23-2016 0:6:12:00 CT",
	        approvedBy: "",
	        approvedDate: "",
	        declineBy: "Jane Smith",
	        declineDate: "05-23-2016 0:6:12:00 CT"
	    }, {
	        name: "John Doe",
	        email: "jdoe@email.com",
	        phone: "+123 456 7890",
	        role: "Producer",
	        status: "DECLINED",
	        requestedDates: "05-23-2016 0:6:12:00 CT",
	        approvedBy: "",
	        approvedDate: "",
	        declineBy: "Jane Smith",
	        declineDate: "05-23-2016 0:6:12:00 CT"
	    }]

	    
	    $scope.selected = [];
	    $scope.isChecked = function () {
	        return $scope.selected.length === $scope.users.length;
	    };
	    $scope.isIndeterminate = function () {
	        return ($scope.selected.length !== 0 &&
                $scope.selected.length !== $scope.users.length);
	    };
	    $scope.toggleAll = function () {
	        if ($scope.selected.length === $scope.users.length) {
	            $scope.selected = [];
	        } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
	            $scope.selected = [];
	            $.each($scope.users, function (index, val) {
	                $scope.selected.push(val)
	            })
	        }
	    };

	    $scope.toggle = function (item) {
	        var idx = $scope.selected.indexOf(item);
	        if (idx > -1) {
	            $scope.selected.splice(idx, 1);
	        }
	        else {
	            $scope.selected.push(item);
	        }
	    };

	    $scope.exists = function (item) {
	        return $scope.selected.indexOf(item) > -1;
	    };
	})
