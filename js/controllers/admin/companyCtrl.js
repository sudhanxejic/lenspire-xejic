﻿angular.module('lenspireApp')
	.controller('CompanyCtrl', function CompanyCtrl($scope, $mdDialog, $mdMedia, $window) {
	    'use strict';

	    $scope.currentNavItem = "company";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.short = function (detail) {
	        $scope.myOrder = detail;
	    }

	    $scope.comp = [{
	        company: "ABC Company",
	        companyEmail: "company@email.com",
	        companyPhone: '123 456 7890',
	        contactPerson: 'Jane Smith',
	        website: 'www.company.com'
	    }, {
	        company: "ABC Company",
	        companyEmail: "company@email.com",
	        companyPhone: '123 456 7890',
	        contactPerson: 'Jane Smith',
	        website: 'www.company.com'
	    }, {
	        company: "ABC Company",
	        companyEmail: "company@email.com",
	        companyPhone: '123 456 7890',
	        contactPerson: 'Jane Smith',
	        website: 'www.company.com'
	    }, {
	        company: "ABC Company",
	        companyEmail: "company@email.com",
	        companyPhone: '123 456 7890',
	        contactPerson: 'Jane Smith',
	        website: 'www.company.com'
	    }, {
	        company: "ABC Company",
	        companyEmail: "company@email.com",
	        companyPhone: '123 456 7890',
	        contactPerson: 'Jane Smith',
	        website: 'www.company.com'
	    }, ]

	    $scope.stat = '  ';

	    $scope.companyCreate = function (ev) {
	        $mdDialog.show({
	            controller: "AddCompanyCtrl",
	            templateUrl: '/partials/admin/addCompany.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true
	        }).then(function (result) {
	            if (result)
	                $scope.comp.push(result);
	        }, function () {
	        });;
	    };

	});
