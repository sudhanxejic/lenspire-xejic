﻿angular.module('lenspireApp')
	.controller('MessagesCtrl', function MessagesCtrl($scope, $mdMedia, $mdSidenav, $mdDialog) {
	    'use strict';
	    $scope.messageLeftSideBar = "/partials/layout/message-left-bar.html";
	    $scope.messageRightSideBar = "/partials/layout/message-right-bar.html";
	    $scope.productionDetailsCollapsed = false;
	    $scope.productionDirectMessageCollapsed = false;
	    $scope.shareDetailsCollapsed = false;
	    $scope.membersDetailsCollapsed = false;
	    $scope.dates = new Date();

	    $scope.sidenavOpen = true;
	    $scope.sidenavOpenright = true;
	    $scope.sidebarToggleLeft = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $scope.sidenavOpenright = false;
	        $mdSidenav("left").toggle()
	    }

	    $scope.sidebarToggleRight = function () {
	        $scope.sidenavOpenright = !$scope.sidenavOpenright;
	        $scope.sidenavOpen = false;
	        $mdSidenav("right").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('gt-sm') },
              function (screenSize) {

                  if (!screenSize) {
                      $scope.screen = false;
                      $scope.sidenavOpenright = false;
                      $scope.sidenavOpen = false;
                  }
                  else {
                      $scope.screen = true;
                  }
              }
             );

	    $scope.productionBook = [{
	        id: '1',
	        bookName: 'Production Book A',
	        members: [{ pic: 'images/data/avatar-2.png', profession: 'model', name: 'Rose Morales' }, { pic: 'images/data/avatar-5.png', profession: 'Assistant', name: 'Jean Gray' }, { pic: 'images/data/avatar-8.png', profession: 'Photographer', name: 'Walter Berry' }]

	    }, {
	        id: '2',
	        bookName: 'Production Book B',
	        members: [{ pic: 'images/data/avatar-3.png', profession: 'Set Designer', name: 'Jacob Mendoza' }, { pic: 'images/data/avatar-6.png', profession: 'Make Up Artist', name: 'Christine Johnston' }]
	    }, {
	        id: '3',
	        bookName: 'Production Book C',
	        members: [{ pic: 'images/data/avatar-4.png', profession: 'Assistant', name: 'Jean Gray' }, { pic: 'images/data/avatar-7.png', profession: 'Photographer', name: 'Walter Berry' }, { pic: 'images/data/avatar-9.png', profession: 'Lighting', name: 'Gary Robertson' }, { pic: 'images/data/avatar-10.png', profession: 'Model', name: 'Terry Hayes' }]
	    }, ];

	    $scope.directMessage = [{
	        id: '1',
	        date: $scope.dates,
	        status: 'Lorem ipsum dolor set amit ...',
	        members: [{
	            pic: 'images/data/avatar-10.png',
	            name: 'Marilyn Salazar'
	        }, {
	            pic: 'images/data/avatar-10.png',
	            name: 'Marilyn Salazar'
	        }]
	    }, {
	        id: '2',
	        date: $scope.dates,
	        status: 'Lorem ipsum dolor set amit ...',
	        members: [{
	            pic: 'images/data/avatar-12.png',
	            name: 'Walter Hansen'
	        }]
	    }, {
	        id: '3',
	        date: $scope.dates,
	        status: 'Lorem ipsum dolor set amit ...',
	        members: [{
	            pic: 'images/data/avatar-1.png',
	            name: 'Catherine Weber'
	        }]
	    }, ]

	    $scope.messager = [{
	        date: ' 05/26/2016 • 8:00 AM',
	        message: [{
	            avatar: 'images/data/avatar-12.png',
	            userid: '1',
	            messages: 'Lorem ipsum dolor sit ametsi, con sectet adipiscing.',
	        }, {
	            avatar: 'images/data/avatar-11.png',
	            userid: '2',
	            messages: 'Lorem ipsum dolor sit ametsi, con sectet adipiscing.',
	        }, {
	            avatar: 'images/data/avatar-10.png',
	            userid: '3',
	            messages: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',
	        }, ]
	    }, {
	        date: ' 04/26/2016 • 8:00 AM',
	        message: [{
	            avatar: 'images/data/avatar-12.png',
	            userid: '1',
	            messages: 'Lorem ipsum dolor sit ametsi, con sectet adipiscing.',
	        }]
	    }, {
	        date: ' 03/26/2016 • 8:00 AM',
	        message: [{
	            avatar: 'images/data/avatar-12.png',
	            userid: '1',
	            messages: 'Lorem ipsum dolor sit ametsi, con sectet adipiscing.',
	        }]
	    }, ]

	    $scope.book2 = [{
	        date: ' 05/26/2016 • 8:00 AM',
	        message: [{
	            userid: '1',
	            messages: 'Lorem ipsum dolor sit ametsi, con sectet adipiscing.',
	        }, {
	            userid: '2',
	            messages: 'Lorem ipsum dolor sit ametsi, con sectet adipiscing.',
	        }, {
	            userid: '3',
	            messages: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',
	        }, ]
	    }, {
	        date: ' 05/26/2016 • 8:00 AM',
	        message: [{
	            userid: '1',
	            messages: 'Lorem ipsum dolor sit ametsi, con sectet adipiscing.',
	        }]
	    }, {
	        date: ' 05/26/2016 • 8:00 AM',
	        message: [{
	            userid: '1',
	            messages: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',
	        }]
	    }, ]

	    $scope.chechSelected = function (id) {
	        for (var l = 0; l < $scope.productionBook.length; l++) {
	            if ($scope.productionBook[l] == id) {
	                return true;
	            } else {
	                return false;
	            }
	        }
	    }

	    $scope.match = function (word) {
	        $scope.selected = word;
	        $scope.personal = null;
	    }

	    $scope.single = function (word) {
	        $scope.personal = word;
	        $scope.selected = null;
	    }


	    $scope.openMessageDialog = function (ev) {
	        $mdDialog.show({
	            controller: "NewMessageCtrl",
	            templateUrl: '/partials/new-message.html',
	            parent: angular.element(document.body),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: true,
	            resolve: {
	                newmessage: function () {
	                    return $scope.productionBook;
	                }
	            }
	        }).then(function (chosenCrews) {
	            $scope.directMessage.push({
	                id: '',
	                date: $scope.dates,
	                status: '',
	                members: chosenCrews
	            });
	            $scope.messager = [];
	        });
	    };


	});