angular.module('lenspireApp')
	.controller('CalendarCtrl', function CalendarCtrl($scope, $filter, $timeout, CommonService, $mdMedia, $mdSidenav) {
	    'use strict';

	    $scope.user = CommonService.getUser();
	    $scope.calensarSideBar = "/partials/productionBook/productions-sidebar.html";

	    $scope.sidenavOpen = true;
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("left").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
              function (screenSize) {
                  $scope.screen = screenSize;
                  if ($scope.screen) {
                      $scope.sidenavOpen = false;
                  }
              }
             );

	    $scope.calendarOptions = {
	        header: {
	            left: 'title',
	            center: '',
	            right: 'prev,next',
	        },
	        buttonIcons: {
	            prev: 'icon icon-chevron-left-4x8',
	            next: 'icon icon-chevron-right-4x8'
	        },
	        height: 570,
	        editable: false,
	        eventRender: function (event, element) {
	            var htmlRender = "";
	            $.each(event.members, function (index, val) {
	                htmlRender += "<img src='" + val.avatar + "'/>";
	            })
	            htmlRender += "<span class='name'>" + event.title + "</span>";
	            element.find(".fc-title").html(htmlRender);
	        }
	    }

	    $scope.crews = [{ id: 1, name: 'Chad Cooper', profile: 'images/data/avatar-10.png' },
            { id: 2, name: 'Heather Greene', profile: 'images/data/avatar-11.png' },
            { id: 3, name: 'Matt Jackson', profile: 'images/data/avatar-12.png' },
            { id: 4, name: 'Lili Regen', profile: 'images/data/avatar-13.png' },
            { id: 5, name: 'Masha Stone', profile: 'images/data/avatar-14.png' }
	    ];
	    var today = new Date();
	    var events = [{
	        title: 'NEIMAN MARCUS',
	        start: new Date(today.getFullYear(), today.getMonth(), 17),
	        end: new Date(today.getFullYear(), today.getMonth(), 22),
	        members: [{ id: 1, avatar: 'images/data/avatar-10.png' }]
	    }, {
	        title: 'ESQUIRE MAGAZINE',
	        start: new Date(today.getFullYear(), today.getMonth(), 8),
	        end: new Date(today.getFullYear(), today.getMonth(), 12),
	        members: [{ id: 2, avatar: 'images/data/avatar-11.png' }, { id: 3, avatar: 'images/data/avatar-12.png' }]
	    }, {
	        title: 'GQ MAGAZINE',
	        start: new Date(today.getFullYear(), today.getMonth(), 8),
	        end: new Date(today.getFullYear(), today.getMonth(), 11),
	        members: [{ id: 4, avatar: 'images/data/avatar-13.png' }]
	    }, {
	        title: 'FLARE MAGAZINE',
	        start: new Date(today.getFullYear(), today.getMonth(), 23),
	        end: new Date(today.getFullYear(), today.getMonth(), 26),
	        members: [{ id: 1, avatar: 'images/data/avatar-10.png' }]
	    }];

	    if ($scope.user.agent)
	        $scope.eventSources = [{
	            events: angular.copy(events)
	        }]
	    else
	        $scope.eventSources = [{
	            events: [{
	                title: 'NEIMAN MARCUS',
	                start: new Date(today.getFullYear(), today.getMonth(), 17),
	                end: new Date(today.getFullYear(), today.getMonth(), 22),
	                members: [{ id: 2, avatar: 'images/data/avatar-1.png' }]
	            }]
	        }];

	    $timeout(function () {
	        $scope.showCalendar = true;
	    }, 0)


	    $scope.checkCerw = function (crew) {
	        return crew.selected;
	    }

	    $scope.selectCerw = function (crew) {
	        $scope.eventSources.events = [];
	        crew.selected = !crew.selected;
	        var selectedArray = $filter('filter')($scope.crews, { selected: true });
	        if (selectedArray.length) {
	            $.each(selectedArray, function (index, _crew) {
	                $.each(events, function (index, val) {
	                    var result = false;
	                    for (var i = 0; i < val.members.length; i++) {
	                        if (_crew.id == val.members[i].id) {
	                            result = true;
	                            break;
	                        }
	                    }
	                    if (result)
	                        $scope.eventSources.events.push(val);
	                });
	            });
	        } else {
	            $scope.eventSources.events = angular.copy(events);
	        }


	        $scope.showCalendar = false;
	        $timeout(function () {
	            $scope.showCalendar = true;
	        }, 0)
	    }

	});
