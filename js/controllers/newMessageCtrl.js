﻿angular.module('lenspireApp')
	.controller('NewMessageCtrl', function NewMessageCtrl($scope, $mdSidenav, newmessage, $mdDialog) {
	    'use strict';

	    $scope.hide = function () {
	        $mdDialog.hide();
	    };
	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };

	    $scope.chosenCrew = function (chosenCrews) {
	        $mdDialog.hide($scope.chosenCrews);
	    };
        
	    $scope.productionBook = [
	       {
	           pic: 'images/data/avatar-2.png',
	           profession: 'model',
	           name: 'Rose Morales'
	       }, {
	           pic: 'images/data/avatar-5.png',
	           profession: 'Assistant',
	           name: 'Jean Gray'
	       },
           {
               pic: 'images/data/avatar-8.png',
               profession: 'Photographer',
               name: 'Walter Berry'
           }, {
               pic: 'images/data/avatar-3.png',
               profession: 'Set Designer',
               name: 'Jacob Mendoza'
           }, {
               pic: 'images/data/avatar-6.png',
               profession: 'Make Up Artist',
               name: 'Christine Johnston'
           }, {
               pic: 'images/data/avatar-4.png',
               profession: 'Assistant',
               name: 'Jean Gray'
           }, {
               pic: 'images/data/avatar-7.png',
               profession: 'Photographer',
               name: 'Walter Berry'
           }, {
               pic: 'images/data/avatar-9.png',
               profession: 'Lighting',
               name: 'Gary Robertson'
           }, {
               pic: 'images/data/avatar-10.png',
               profession: 'Model',
               name: 'Terry Hayes'
           }
	    ];
	    $scope.chosenCrews = [];
	    $scope.selected = function (value, $index, book) {
	        $scope.chosenCrews.push(value);
	        $scope.search = "";
	        $("#terms").focus();
	        book.splice($index, 1);
	    }

	    $scope.remove = function ($index, chosenCrew) {
	        $scope.chosenCrews.splice($index, 1);
	        $scope.productionBook.push(chosenCrew);
	    }

	    $scope.focus = function () {
	        $("#terms").focus();
	    }
	});