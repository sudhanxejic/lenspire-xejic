﻿angular.module('lenspireApp')
	.controller('BillingCtrl', function BillingCtrl($scope, $location, $window, CommonService) {
	    'use strict';
	    $scope.currentNavItem = "billing";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.upgradeSetp = 1;

	    $scope.account = {
	        upgrade: true,
	        plan: {
	            name: 'annually',
	            month: '$20',
	            charge: '$240'
	        }
	    }

	    $scope.upgradeNow = function () {
	        $scope.upgradeSetp = 2;
	    }

	    $scope.buyNow = function () {
	        $scope.account.upgrade = true;
	        $scope.account.plan = {
	            name: 'annually',
	            month: '$20',
	            charge: '$240'
	        }

	    }

	    $scope.changePlan = function () {
	        $scope.account.upgrade = false;
	        $scope.upgradeSetp = 2;
	    }

	    $scope.unSubscribe = function () {
	        $scope.upgradeSetp = 1;
	        $scope.account.upgrade = false;
	        $scope.account.plan = {
	            name: 'annually',
	            month: '$20',
	            charge: '$240'
	        }
	    }

	    $scope.unclaim = [{
	        avatar: 'images/data/photo-1.png',
	        name: 'Violet River',
	        title: 'South East Asia Photoshoot',
	        role: 'Photographer'
	    },
        {
            avatar: 'images/data/photo-2.png',
            name: 'Plaza Water',
            title: 'Ceci Summer 2016',
            role: 'Photographer'
        }];

	});