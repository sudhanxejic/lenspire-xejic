﻿angular.module('lenspireApp')
	.controller('AccountCtrl', function AccountCtrl($scope, $location, $window, CommonService) {
	    'use strict';
	    $scope.currentNavItem = "account";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.addToggleVal = false;
	    $scope.roles = CommonService.getCrews();
	    $scope.user = CommonService.getUser();
	    $scope.newAgent = {
	        agents: [],
	        message: null
	    }
	    $scope.newEmployee = {
	        employees: [],
	        message: null
	    }
	    $scope.newArtist = {
	        artists: [],
	        message: null
	    }

	    $scope.roleTransformChip = function (chip) {
	        if (angular.isObject(chip)) {
	            return chip;
	        }
	    }

	    $scope.agentTransformChip = function (chip) {
	        if (angular.isObject(chip)) {
	            return chip;
	        } else {
	            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	            if (re.test(chip))
	                return { name: chip };
	            else
	                return null;
	        }
	    }

	    $scope.agents = [{
	        name: "Amy Agent",
	        avatar: 'images/data/avatar-11.png',
	        status: 'completed',
	        phoneNumber: '(123) 456 7890',
	        organization: 'BRYNN CREATIVE'
	    }, {
	        name: "Adam Agent",
	        avatar: 'images/data/avatar-13.png',
	        status: 'pending',
	        phoneNumber: '(123) 456 7890',
	        organization: 'BRYNN CREATIVE'
	    }];

	    $scope.agents = [{
	        name: "Amy Agent",
	        avatar: 'images/data/avatar-11.png',
	        status: 'completed',
	        phoneNumber: '(123) 456 7890',
	        organization: 'BRYNN CREATIVE'
	    }, {
	        name: "Adam Agent",
	        avatar: 'images/data/avatar-13.png',
	        status: 'pending',
	        phoneNumber: '(123) 456 7890',
	        organization: 'BRYNN CREATIVE'
	    }];

	    $scope.artists = [{
	        name: "Matt Jackson",
	        avatar: 'images/data/avatar-12.png',
	        status: 'completed',
	        phoneNumber: '(123) 456 7890',
	        role: 'Male Model'
	    }, {
	        name: "Chad Cooper",
	        avatar: 'images/data/avatar-14.png',
	        status: 'pending',
	        phoneNumber: '(123) 456 7890',
	        role: 'Photographer'
	    }];

	    $scope.employees = [{
	        name: "Matt Jackson",
	        avatar: 'images/data/avatar-12.png',
	        status: 'completed',
	        phoneNumber: '(123) 456 7890',
	        role: 'Male Model'
	    }, {
	        name: "Chad Cooper",
	        avatar: 'images/data/avatar-14.png',
	        status: 'pending',
	        phoneNumber: '(123) 456 7890',
	        role: 'Photographer'
	    }];

	    $scope.organizations = [{
	        avatar: "images/data/company-logo.png",
	        name: 'BRYNN CREATIVES',
	        email: 'contact@brynncreatives',
	        website: 'brynncreative.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }, {
	        avatar: null,
	        name: 'Sample company',
	        email: 'contact@sample.com',
	        website: 'sample.com',
	    }]

	    $scope.newOrganization = function () {
	        $scope.openNewOrganization = true;
	    }
	    $scope.inviteOrganization = function () {
	        $scope.openNewOrganization = false;
	    }

	    $scope.selectedOrganization = function (organization) {	        
	        $scope.account.organizationName = organization.name;
	        $scope.account.organizationEmail = organization.email;
	        $scope.account.organizationWebsite = organization.website;
	        $scope.openNewOrganization = false;
	    }

	    $scope.removeAgent = function (index) {
	        $scope.account.agents.splice(index, 1);
	    }
	    $scope.removeArtist = function (index) {
	        $scope.account.artists.splice(index, 1);
	    }
	    $scope.removeEmployee = function (index) {
	        $scope.account.employees.splice(index, 1);
	    }

	    $scope.addToggle = function () {
	        $scope.addToggleVal = !$scope.addToggleVal;
	    }

	    $scope.sendArtistInvite = function () {
	        $.each($scope.newArtist.artists, function (index, agent) {
	            $scope.account.artists.push({
	                name: agent.name,
	                avatar: null,
	                status: 'pending',
	                phoneNumber: null,
	                role: ''
	            })
	        });
	        $scope.newArtist.artists = [];
	    }
	    $scope.sendEmployeeInvite = function () {
	        $.each($scope.newEmployee.employees, function (index, agent) {
	            $scope.account.employees.push({
	                name: agent.name,
	                avatar: null,
	                status: 'pending',
	                phoneNumber: null,
	                role: ''
	            })
	        });
	        $scope.newEmployee.employees = [];
	    }
	    $scope.sendAgentInvite = function () {
	        $.each($scope.newAgent.agents, function (index, agent) {
	            $scope.account.agents.push({
	                name: agent.name,
	                avatar: null,
	                status: 'pending',
	                phoneNumber: null,
	                organization: ''
	            })
	        });
	        $scope.newAgent.agents = [];
	    }

	    if ($scope.user.artist) {
	        $scope.account = {
	            avatar: "images/data/artists-avatar.png",
	            firstName: 'Chad Cooper',
	            lastName: 'Chad Cooper',
	            about: "I am a photographer based in San Francisco Bay Area. My specialties include wedding and night life.",
	            email: 'chadcooper@email.com',
	            phoneNumber: '123 456 7890',
	            socialMedia: 'instagram.com/chadcooper',
	            organizationName: 'Cooper Photography',
	            organizationEmail: 'contact@cooperphotography.com',
	            organizationWebsite: 'cooperphotography.com',
	            roles: [{ id: 98, name: "Photographer" }, { id: 12, name: "Art Director" }],
	            agents: [{
	                name: "Amy Agent",
	                avatar: 'images/data/avatar-11.png',
	                status: 'completed',
	                phoneNumber: '(123) 456 7890',
	                organization: 'BRYNN CREATIVE'
	            }, {
	                name: "Adam Agent",
	                avatar: 'images/data/avatar-13.png',
	                status: 'pending',
	                phoneNumber: '(123) 456 7890',
	                organization: 'BRYNN CREATIVE'
	            }, {
	                name: "chad@email.com",
	                avatar: null,
	                status: 'pending',
	                phoneNumber: null,
	                organization: ''
	            }]
	        };
	    } else if ($scope.user.agent && !$scope.user.organization) {
	        $scope.account = {
	            avatar: "images/data/company-logo.png",
	            firstName: 'Adam',
	            lastName: 'Agent',
	            about: 'I am a modeling and talent agent based in New York City.',
	            
	            email: 'adamagent@email.com',
	            phoneNumber: '123 456 7890',
	            socialMedia: 'instagram.com/adam',
	            organizationName: 'BRYNN CREATIVES',
	            organizationEmail: 'contact@brynncreatives',
	            organizationWebsite: 'brynncreative.com',
	            roles: [{ id: 163, name: "Modeling Agent" }, { id: 148, name: "Talent Agent" }],
	            artists: [{
	                name: "Matt Jackson",
	                avatar: 'images/data/avatar-12.png',
	                status: 'completed',
	                phoneNumber: '(123) 456 7890',
	                role: 'Male Model'
	            }, {
	                name: "Chad Cooper",
	                avatar: 'images/data/avatar-14.png',
	                status: 'pending',
	                phoneNumber: '(123) 456 7890',
	                role: 'Photographer'
	            }, {
	                name: "chad@email.com",
	                avatar: null,
	                status: 'pending',
	                phoneNumber: null,
	                role: ''
	            }]
	        };
	    } else if ($scope.user.organization) {
	        $scope.account = {
	            avatar: "images/data/company-logo.png",
	            displayName: 'BRYNN CREATIVES',
	            about: 'Brynn Creative is a full service production company specializing in print advertising. Our goal is to serve each client with efficiency and budget consciousness while maintaining the creative vision. In order to B Creative, we believe any production issue is never referred to as a problem, but it is viewed as an "interesting idea" needing a creative solution. Let us entertain every "interesting idea" within each production.',
	            address: '1500 Marilla St, Dallas, TX 75201',
	            email: 'info@brynncreative.com',
	            phoneNumber: '214.673.5496',
	            fax: '214.594.8060',
	            socialMedia: 'instagram.com/brynncreative.com',
	            website: 'www.brynncreative.com',
	            roles: [{ id: 162, name: "Full Service Production" }],
	            employees: [{
	                name: "Matt Jackson",
	                avatar: 'images/data/avatar-12.png',
	                status: 'completed',
	                phoneNumber: '(123) 456 7890',
	                role: 'Male Model'
	            }, {
	                name: "Chad Cooper",
	                avatar: 'images/data/avatar-14.png',
	                status: 'pending',
	                phoneNumber: '(123) 456 7890',
	                role: 'Photographer'
	            }, {
	                name: "chad@email.com",
	                avatar: null,
	                status: 'pending',
	                phoneNumber: null,
	                role: ''
	            }]
	        };
	    }

	});