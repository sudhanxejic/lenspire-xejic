angular.module('lenspireApp')
	.controller('EnlargedCtrl', function EnlargedCtrl($scope, $mdDialog, $mdMedia, $timeout, feed) {
	    $scope.feed = feed;
	    $scope.close = function () {
	        $mdDialog.hide();
	    };
	    var carousel = null;
	    $timeout(function () {
	        carousel = $('#carousel').carousel({
	            interval: false
	        });
	    });
	    $scope.carouselNext = function () {
	        carousel.carousel('next')
	    }
	    $scope.carouselprev = function () {
	        carousel.carousel('prev')
	    }

	    $scope.credits = [{
	        id: 1,
	        avatar: 'images/data/avatar-1.png',
	        role: "Art Director",
	        name: "Andrea Lewis",
	        pending: false
	    }, {
	        id: 2,
	        avatar: 'images/data/avatar-2.png',
	        role: "Assistant",
	        name: "Larry Carr",
	        pending: false
	    }, {
	        id: 3,
	        avatar: 'images/data/avatar-3.png',
	        role: "Fashion Director",
	        name: "Craig Mendoza",
	        pending: false
	    }, {
	        id: 4,
	        avatar: 'images/data/avatar-4.png',
	        role: "Photographer",
	        name: "Matt Jackson",
	        pending: false
	    }, {
	        id: 5,
	        avatar: 'images/data/avatar-5.png',
	        role: "Set Designer",
	        name: "Ben Washington",
	        pending: false
	    }, {
	        id: 6,
	        avatar: 'images/data/avatar-6.png',
	        role: "Model",
	        name: "Jane Wu",
	        pending: false
	    }, {
	        id: 7,
	        avatar: 'images/data/avatar-7.png',
	        role: "Make Up Artist",
	        name: "Jean Warren",
	        pending: true
	    }];

	    $scope.lists = [{
	        avatar: 'images/data/avatar-1.png',
	        role: 'Photographer',
	        name: 'Carly',
	    }, {
	        avatar: 'images/data/avatar-2.png',
	        role: 'Makeup Artist',
	        name: 'Dalia ',
	        email: ''
	    }, {
	        avatar: 'images/data/avatar-3.png',
	        role: 'Assistant',
	        name: 'Kenwood',
	    }, {
	        avatar: 'images/data/avatar-4.png',
	        role: 'Fashion Director',
	        name: 'Cooper',

	    }];

	    $scope.openNewArtist = false;
	    $scope.newArtist = {

	    }
	    $scope.openNewArtistFun = function () {
	        $scope.openNewArtist = !$scope.openNewArtist;
	    }

	    $scope.addCredit = function (_artist) {
	        var artist = angular.copy(_artist)
	        artist.id = $scope.credits.lenght + 1;
	        artist.pending = false;
	        $scope.credits.push(artist);
	    }

	    $scope.addNewArtist = function () {
	        if ($scope.newArtist.name) {
	            var newArtist = angular.copy($scope.newArtist);
	            newArtist.id = $scope.credits.lenght + 1;
	            newArtist.avatar = 'images/data/avatar-7.png',
                newArtist.pending = true;
	            $scope.credits.push(newArtist);
	            $scope.newArtist = null;
	            $scope.openNewArtist = false;
	        }
	    }

	    $scope.removecredit = function (idx) {
	        $scope.credits.splice(idx, 1);
	    };

	    $scope.addToWinkboard = function (ev, feed) {
	        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
	        $mdDialog.show({
	            controller: 'WinkBoardAddCtrl',
	            templateUrl: '/partials/winkBoard/winkboard-add.html',
	            parent: angular.element('body'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: useFullScreen,
	            resolve: {
	                feed: function () {
	                    return feed;
	                }
	            }
	        })
            .then(function (response) {

            }, function () {

            });
	    }
	})