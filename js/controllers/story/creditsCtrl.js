angular.module('lenspireApp')
	.controller('CreditsCtrl', function CreditsCtrl($scope, $mdMedia, $mdSidenav, $window, CommonService) {
	    'use strict';
	    $scope.currentNavItem = "credits";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.sidenavOpen = true;
	    $scope.user = CommonService.getUser();
	    if ($scope.user.agent)
	        $scope.sideBar = "/partials/layout/agent-sidebar.html";
	    else
	        $scope.sideBar = "/partials/layout/artist-sidebar.html";
	   
	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("right").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
             function (screenSize) {
                 $scope.screen = screenSize;
                 if ($scope.screen == true) {
                     $scope.sidenavOpen = false;
                 }
             }
            );

	    $scope.credits1 = [{
	        image: "images/data/avatar-1.png",
	        job: "Publisher",
	        members: ["Cosmo Usa"]
	    }, {
	        image: "images/data/avatar-2.png",
	        job: "Client",
	        members: ["John Doe"]
	    }, {
	        image: "images/data/avatar-3.png",
	        job: "agency",
	        members: ["Brynn Creative"]
	    }];

	    $scope.credits2 = [{
	        image: "images/data/avatar-4.png",
	        job: "Art Director",
	        members: ["Andrea Lewis"]
	    }, {
	        image: "images/data/avatar-5.png",
	        job: "Assistant",
	        members: ["Larry Carr"]
	    }, {
	        image: "images/data/avatar-6.png",
	        job: "Fashion Director",
	        members: ["Jane Wu"]
	    }, {
	        image: "images/data/avatar-7.png",
	        job: "Makeup Artist",
	        members: ["Sharon Woods"]
	    }, {
	        image: "images/data/avatar-8.png",
	        job: "Photographer",
	        members: ["Dalia Kenwood"]
	    }, {
	        image: "images/data/avatar-9.png",
	        job: "Set Designer",
	        members: ["Ben Washington"]
	    }, {
	        image: "images/data/avatar-10.png",
	        job: "Model",
	        members: ["Amanda Shaw"]
	    }]

	});
