angular.module('lenspireApp')
	.controller('StoryCtrl', function StoryCtrl($scope, $mdSidenav, $mdMedia, $window, $mdDialog, CommonService) {
	    'use strict';
	    $scope.currentNavItem = "story";
	    $scope.clickNavigation = function (navItem) {
	        $window.location.href = navItem;
	    }
	    $scope.user = CommonService.getUser();
	    if ($scope.user.agent)
	        $scope.sideBar = "/partials/layout/agent-sidebar.html";
	    else
	        $scope.sideBar = "/partials/layout/artist-sidebar.html";
	    $scope.sidenavOpen = true;

	    $scope.sidebarToggle = function () {
	        $scope.sidenavOpen = !$scope.sidenavOpen;
	        $mdSidenav("right").toggle()
	    }

	    $scope.$watch(function () { return $mdMedia('xs') },
             function (screenSize) {
                 $scope.screen = screenSize;
                 if ($scope.screen == true) {
                     $scope.sidenavOpen = false;
                 }
             }
            );

	    $scope.openEnlarged = function (ev, feed) {
	        $mdDialog.show({
	            controller: 'EnlargedCtrl',
	            templateUrl: '/partials/story/enlarged.html',
	            parent: angular.element('body'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: false,
	            resolve: {
	                feed: function () {
	                    return feed;
	                }
	            }
	        })
            .then(function (response) {

            }, function () {

            });
	    }

	    $scope.addToWinkboard = function (ev, feed) {
	        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
	        $mdDialog.show({
	            controller: 'WinkBoardAddCtrl',
	            templateUrl: '/partials/winkBoard/winkboard-add.html',
	            parent: angular.element('#page-content'),
	            targetEvent: ev,
	            clickOutsideToClose: true,
	            fullscreen: useFullScreen,
	            resolve: {
	                feed: function () {
	                    return feed;
	                }
	            }
	        })
            .then(function (response) {

            }, function () {

            });
	    }

	    $scope.feeds = [{
	        image: "images/data/photo-1.png",
	        publishers: [{
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-1.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-2.png"
	        }],
	        winkName: "Fall Collection",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-2.png",
	        publishers: [{
	            publisherName: "National Geographics",
	            publisherAvatar: "images/data/avatar-3.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-4.png"
	        }, {
	            publisherName: "People",
	            publisherAvatar: "images/data/avatar-5.png"
	        }],
	        winkName: "Violet River",
	        active: false,
	        public: true
	    }, {
	        image: "images/data/photo-3.png",
	        publishers: [{
	            publisherName: "People",
	            publisherAvatar: "images/data/avatar-6.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-7.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-8.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-9.png"
	        }, {
	            publisherName: "Cosmopolitan",
	            publisherAvatar: "images/data/avatar-10.png"
	        }],
	        winkName: "Lost in Nature",
	        active: false,
	        public: true
	    }];

	});
